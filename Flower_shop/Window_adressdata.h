#pragma once
#include <QDialog>
#include <QString>
#include <QSqlDatabase>
#include <QtSql>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include "ui_AdressData.h"
#include "Users.h"
class Window_adressdata :
	public QDialog
{
	Q_OBJECT

public:
	explicit Window_adressdata(QDialog *parent = Q_NULLPTR, QString mode = NULL, Users* users_data = NULL);

private:
	Ui::AdressData ui;
	Users* users_data;
	QString mode;

private slots:
	bool on_okButton_clicked();
}