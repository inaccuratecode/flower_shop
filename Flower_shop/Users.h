#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>
#include <QDebug>
#include <QSqlQuery>

class Users
{
	QSqlDatabase* database;
	QSqlQuery* query;
	QSqlQuery* extra_query;
	QString active_user_login = "";
	int active_user_adress = 0;
	char active_user_type;
	bool status = false;

public:
	bool login(QString login, QString password);
	bool login_status();
	bool reset_password();
	char get_user_type();
	bool add_user(QString login, QString password, QString email, QString question, QString answer);
	bool get_users(QListWidget *list);
	bool add_adress(QString login, QString street, QString city, int postcode, QString phone);
	std::vector<QString>* get_adress_by_ID(int ID, std::vector<QString>* data);
	bool edit_adress(int ID, QString street, QString city, int postcode, QString phone);
	bool update_active_adress(QString login, int adress_ID);
	bool get_user_adresses(QString login, QListWidget* list);
	bool set_user_delete(QString login, bool state);
	std::vector<QString>* get_user_data(QString login, std::vector<QString>* data);
	bool edit_user(QString login, QString password, QString email, QString question, QString answer);
	bool edit_password(QString login, QString password);
	bool change_user_type(QString login);
	bool clear_users();
	QString get_question(QString login);
	int get_user_active_adress();
	bool compare_answer_questrion(QString login, QString question, QString answer);
	int get_order_count_at(QString login, QDate date);
	int get_amount_of_products_at(QString login, QDate date);
	QString get_active_user_login();
	Users(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query);
	~Users();


};

