#include "stdafx.h"
#include "database_manager.h"

database_manager::database_manager(QString host_name, QString db_name, int port, QString user_name, QString user_password)
{
	database.setHostName(host_name);
	database.setDatabaseName(db_name);
	database.setPort(port);
	database.setUserName(user_name);

	if (user_password.length() != 0)
		database.setPassword(user_password);
}

//Database connector
bool database_manager::Connect_to_db()
{
	//Connecting to database and getting status of connection
	connected = database.open();
	qDebug() << "Database connection status = " << connected;

	if (database.lastError().isValid()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!\n" + database.lastError().text());
		connected = false;
		return connected;
	}
	return connected;
}

//Database connection status getter
bool database_manager::connection_status()
{
	return connected;
}

//Database pointer getter
QSqlDatabase * database_manager::ptr_to_db()
{
	QSqlDatabase* ptr_do_db = &database;
	return ptr_do_db;
}

//Database query pointer getter
QSqlQuery* database_manager::ptr_to_query()
{
	QSqlQuery* ptr = &query;
	return ptr;
}

//Database additional query pointer getter
QSqlQuery * database_manager::ptr_to_extra_query()
{
	QSqlQuery* ptr = &extra_query;
	return ptr;
}

database_manager::~database_manager()
{
}
