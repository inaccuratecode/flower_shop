#include "stdafx.h"
#include "Window_Product_Data.h"
#include <QFileDialog>
#include <QDir>

//Main constructor that make sure only edit lines specified by mode are enabled
Window_Product_Data::Window_Product_Data(QDialog *parent, QString mode, Products* products_data, int ID)
{
	this->ID = ID;
	this->mode = mode;
	this->products_data = products_data;
	ui.setupUi(this);

	if (mode == "edit") {
		std::vector<QString> current;
		products_data->get_info_from_ID(&current, ID);
		if (current.size() != 4) 
			QMessageBox::warning(NULL, "ERROR", "Error while getting current product info!");
		else {
			ui.lineEdit_name->setText(current.at(0));
			ui.lineEdit_category->setText(current.at(1));
			ui.lineEdit_photo_directory->setText(current.at(3)+".jpg");
			ui.doubleSpinBox_price->setValue(current.at(2).toFloat());
		}
	}

}

//Opening windows explorer to get photo directory
void Window_Product_Data::on_pushButton_directory_clicked() {
	QString directory, filter;
	filter = "JPEG (*.jpg)";
	directory = QFileDialog::getOpenFileName(this, "Select Picture", QDir::homePath(),filter);
	ui.lineEdit_photo_directory->setText(directory);
}

void Window_Product_Data::on_okButton_clicked()
{
	if (mode == "add") {
		QString name, category, photo_direcotry;
		float price;

		name = ui.lineEdit_name->text();
		category = ui.lineEdit_category->text();
		photo_direcotry = ui.lineEdit_photo_directory->text();
		price = ui.doubleSpinBox_price->value();

		if (name.length() == 0 || category.length() == 0 || photo_direcotry.length() == 0) {
			QMessageBox::warning(NULL, "ERROR", "Data input is not complete!");
			return;
		}
		else {
			products_data->add_product(name, category, price, photo_direcotry);
		}
	}
	else {
		QString name, category, photo_direcotry;
		float price;

		name = ui.lineEdit_name->text();
		category = ui.lineEdit_category->text();
		photo_direcotry = ui.lineEdit_photo_directory->text();
		price = ui.doubleSpinBox_price->value();

		if (name.length() == 0 || category.length() == 0 || photo_direcotry.length() == 0) {
			QMessageBox::warning(NULL, "ERROR", "Data input is not complete!");
			return;
		}
		else {
			products_data->edit_product(ID, name, category, price, photo_direcotry);
		}
	}
		
}
