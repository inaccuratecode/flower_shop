#include "stdafx.h"
#include "Orders.h"
#include <QDateTime>

//Main constructor that provide acess to users data and query through pointers to objects
Orders::Orders(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query, Users* users_data)
{
	this->database = database;
	this->query = query;
	this->extra_query = extra_query;
	this->users_data = users_data;
}

//Geting orders from database and putting them in list widget
bool Orders::get_orders(QListWidget * list)
{
	QListWidgetItem* itm;
	QString data;
	QSqlQuery tmp_query;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Getting order data form database through query
	query->clear();
	query->prepare("SELECT `order_ID`, `user_login`, `adress_ID`, `status`, `order_date`, `price_all` FROM `orders` WHERE 1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();

	//Loop to put order data in list
	while (query->next()) {
		itm = new QListWidgetItem;
		extra_query->prepare("SELECT `street`, `postcode`, `city`, `phone` FROM `adress` WHERE ID=?");
		extra_query->bindValue(0, query->value(2).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}
		extra_query->next();

		//Creating description of order
		data = "ID: " + query->value(0).toString() + "\nUser: " + query->value(1).toString();
		data.append("\nAdress: \n\tStreet: " + extra_query->value(0).toString() + "\n\tCity: " + extra_query->value(2).toString() + " - " + extra_query->value(1).toString() + "\n\tPhone: " + extra_query->value(3).toString());
		data.append("\nStatus: " + query->value(3).toString() + "\nDate: " + query->value(4).toString() + "\nOrder price: " + query->value(5).toString() + "\nOrder content:");

		extra_query->prepare("SELECT `product_ID`, `amount` FROM `amount` WHERE order_ID=?");
		extra_query->bindValue(0, query->value(0).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}
		
		for (int x = 0; extra_query->next(); x++) {
			tmp_query.prepare("SELECT `name`, `price` FROM `products` WHERE ID=?");
			tmp_query.bindValue(0, extra_query->value(0));
			if (!tmp_query.exec()) {
				QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
				return false;
			}
			tmp_query.next();
			data.append("\n\t#" + QString::fromStdString(std::to_string(x)) + ": " + tmp_query.value(0).toString() + "\n\tProduct ID: "+ extra_query->value(0).toString() + "\n\tPrice per pice: " + tmp_query.value(1).toString() + "\n\tAmount ordered: " + extra_query->value(1).toString());
			data.append("\n______________________________");

		}
		itm->setText(data);
		list->addItem(itm);
	}

	return true;
}

//Overloaded function to get orders from database with filter
bool Orders::get_orders(QListWidget * list, QString filter)
{
	QListWidgetItem* itm;
	QString data;
	QSqlQuery tmp_query;

	if (filter == "all") {
		bool out = get_orders(list);
		return out;
	}

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->clear();
	query->prepare("SELECT `order_ID`, `user_login`, `adress_ID`, `status`, `order_date`, `price_all` FROM `orders` WHERE status=?");
	query->bindValue(0, filter);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		itm = new QListWidgetItem;
		extra_query->prepare("SELECT `street`, `postcode`, `city`, `phone` FROM `adress` WHERE ID=?");
		extra_query->bindValue(0, query->value(2).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}
		extra_query->next();

		data = "ID: " + query->value(0).toString() + "\nUser: " + query->value(1).toString();
		data.append("\nAdress: \n\tStreet: " + extra_query->value(0).toString() + "\n\tCity: " + extra_query->value(2).toString() + " - " + extra_query->value(1).toString() + "\n\tPhone: " + extra_query->value(3).toString());
		data.append("\nStatus: " + query->value(3).toString() + "\nDate: " + query->value(4).toString() + "\nOrder price: " + query->value(5).toString() + "\nOrder content:");

		extra_query->prepare("SELECT `product_ID`, `amount` FROM `amount` WHERE order_ID=?");
		extra_query->bindValue(0, query->value(0).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}

		for (int x = 0; extra_query->next(); x++) {
			tmp_query.prepare("SELECT `name`, `price` FROM `products` WHERE ID=?");
			tmp_query.bindValue(0, extra_query->value(0));
			if (!tmp_query.exec()) {
				QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
				return false;
			}
			tmp_query.next();
			data.append("\n\t#" + QString::fromStdString(std::to_string(x)) + ": " + tmp_query.value(0).toString() + "\n\tProduct ID: " + extra_query->value(0).toString() + "\n\tPrice per pice: " + tmp_query.value(1).toString() + "\n\tAmount ordered: " + extra_query->value(1).toString());
			data.append("\n______________________________");

		}
		itm->setText(data);
		list->addItem(itm);
	}

	return true;
}

//Getting specified user orders from database and putting them in list widget
bool Orders::get_user_orders(QListWidget * list, QString login)
{
	QListWidgetItem* itm;
	QString data;
	QSqlQuery tmp_query;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Getting data from database
	query->clear();
	query->prepare("SELECT `order_ID`, `user_login`, `adress_ID`, `status`, `order_date`, `price_all` FROM `orders` WHERE user_login=?");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		itm = new QListWidgetItem;
		extra_query->prepare("SELECT `street`, `postcode`, `city`, `phone` FROM `adress` WHERE ID=?");
		extra_query->bindValue(0, query->value(2).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}
		extra_query->next();

		//Creating description of order
		data = "ID: " + query->value(0).toString() + "\nUser: " + query->value(1).toString();
		data.append("\nAdress: \n\tStreet: " + extra_query->value(0).toString() + "\n\tCity: " + extra_query->value(2).toString() + " - " + extra_query->value(1).toString() + "\n\tPhone: " + extra_query->value(3).toString());
		data.append("\nStatus: " + query->value(3).toString() + "\nDate: " + query->value(4).toString() + "\nOrder price: " + query->value(5).toString() + "\nOrder content:");

		extra_query->prepare("SELECT `product_ID`, `amount` FROM `amount` WHERE order_ID=?");
		extra_query->bindValue(0, query->value(0).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}

		for (int x = 0; extra_query->next(); x++) {
			tmp_query.prepare("SELECT `name`, `price` FROM `products` WHERE ID=?");
			tmp_query.bindValue(0, extra_query->value(0));
			if (!tmp_query.exec()) {
				QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
				return false;
			}
			tmp_query.next();
			data.append("\n\t#" + QString::fromStdString(std::to_string(x)) + ": " + tmp_query.value(0).toString() + "\n\tProduct ID: " + extra_query->value(0).toString() + "\n\tPrice per pice: " + tmp_query.value(1).toString() + "\n\tAmount ordered: " + extra_query->value(1).toString());
			data.append("\n______________________________");

		}
		itm->setText(data);
		list->addItem(itm);
	}

	return true;
}

//Finilizing order from cart
bool Orders::order_from_cart(QListWidget * list, float cart_value)
{
	QDateTime date;
	QString tmp_ID, amount;
	QSqlQuery tmp_query;
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	if (list->count() == 0) {
		QMessageBox::warning(NULL, "ERROR", "Cart is empty!");
		return false;
	}

	if (users_data->get_user_active_adress() == 0) {
		QMessageBox::warning(NULL, "ERROR", "Active adress is not set!");
		return false;
	}

	//Updating database
	query->prepare("INSERT INTO `orders`(`user_login`, `adress_ID`, `order_date`, `price_all`) VALUES (?,?,?,?)");
	query->bindValue(0, users_data->get_active_user_login());
	query->bindValue(1, users_data->get_user_active_adress());
	query->bindValue(2, date.currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	query->bindValue(3, cart_value);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	//Getting order ID generated by sequence in database
	query->prepare("SELECT MAX(`order_ID`) FROM `orders` WHERE `user_login`=?");
	query->bindValue(0, users_data->get_active_user_login());
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	query->next();

	//Adding ordered products to table
	for (int x = 0; x < list->count(); x++) {
		tmp_ID = list->item(x)->text();
		tmp_ID = tmp_ID.section("\n", 1, 1);
		tmp_ID = tmp_ID.section(" ", 1, 1);

		amount = list->item(x)->text();
		amount = amount.section("\n", 2, 2);
		amount = amount.section(" ", 2, 2);

		//Updating database
		extra_query->prepare("INSERT INTO `amount`(`order_ID`, `product_ID`, `amount`) VALUES (?,?,?)");
		extra_query->bindValue(0, query->value(0).toInt());
		extra_query->bindValue(1, tmp_ID);
		extra_query->bindValue(2, amount);
		if (!extra_query->exec())
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());

		//Updating quantity of available product in database
		tmp_query.prepare("SELECT  `amount` FROM `products` WHERE `ID`=?");
		tmp_query.bindValue(0, tmp_ID);
		if (!tmp_query.exec())
			QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
		tmp_query.next();

		extra_query->prepare("UPDATE `products` SET `amount`=? WHERE `ID`=?");
		extra_query->bindValue(0, (tmp_query.value(0).toInt() - amount.toInt()));
		extra_query->bindValue(1, tmp_ID);
		if (!extra_query->exec())
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
	}
	list->clear();
	QMessageBox::information(NULL, "Order", "Successfully ordered items from the cart!");
	return true;
}

//Updating order status in database
bool Orders::update_order_status(int ID, QString status)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->prepare("UPDATE `orders` SET `status`=? WHERE `order_ID`=?");
	query->bindValue(0, status);
	query->bindValue(1, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QMessageBox::information(NULL, "Order", "Order status changed");
	return true;
}

Orders::~Orders()
{
}
