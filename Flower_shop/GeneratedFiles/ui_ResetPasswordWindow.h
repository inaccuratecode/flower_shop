/********************************************************************************
** Form generated from reading UI file 'ResetPasswordWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESETPASSWORDWINDOW_H
#define UI_RESETPASSWORDWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ResetPasswordWindow
{
public:
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit_password;
    QLineEdit *lineEdit_confirm;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *okButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *ResetPasswordWindow)
    {
        if (ResetPasswordWindow->objectName().isEmpty())
            ResetPasswordWindow->setObjectName(QString::fromUtf8("ResetPasswordWindow"));
        ResetPasswordWindow->resize(358, 120);
        horizontalLayout_3 = new QHBoxLayout(ResetPasswordWindow);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(ResetPasswordWindow);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(ResetPasswordWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lineEdit_password = new QLineEdit(ResetPasswordWindow);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));
        lineEdit_password->setEchoMode(QLineEdit::Password);

        verticalLayout->addWidget(lineEdit_password);

        lineEdit_confirm = new QLineEdit(ResetPasswordWindow);
        lineEdit_confirm->setObjectName(QString::fromUtf8("lineEdit_confirm"));
        lineEdit_confirm->setEchoMode(QLineEdit::Password);

        verticalLayout->addWidget(lineEdit_confirm);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        okButton = new QPushButton(ResetPasswordWindow);
        okButton->setObjectName(QString::fromUtf8("okButton"));

        horizontalLayout_2->addWidget(okButton);

        cancelButton = new QPushButton(ResetPasswordWindow);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout_2->addWidget(cancelButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        horizontalLayout_3->addLayout(verticalLayout_3);


        retranslateUi(ResetPasswordWindow);
        QObject::connect(okButton, SIGNAL(clicked()), ResetPasswordWindow, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), ResetPasswordWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(ResetPasswordWindow);
    } // setupUi

    void retranslateUi(QDialog *ResetPasswordWindow)
    {
        ResetPasswordWindow->setWindowTitle(QApplication::translate("ResetPasswordWindow", "Dialog", nullptr));
        label->setText(QApplication::translate("ResetPasswordWindow", "New Password", nullptr));
        label_2->setText(QApplication::translate("ResetPasswordWindow", "Confirm Password", nullptr));
        okButton->setText(QApplication::translate("ResetPasswordWindow", "OK", nullptr));
        cancelButton->setText(QApplication::translate("ResetPasswordWindow", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ResetPasswordWindow: public Ui_ResetPasswordWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESETPASSWORDWINDOW_H
