/********************************************************************************
** Form generated from reading UI file 'ProductData.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRODUCTDATA_H
#define UI_PRODUCTDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProductData
{
public:
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer_5;
    QLabel *label_2;
    QSpacerItem *verticalSpacer_6;
    QLabel *label_3;
    QSpacerItem *verticalSpacer_7;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit_name;
    QSpacerItem *verticalSpacer_4;
    QLineEdit *lineEdit_category;
    QSpacerItem *verticalSpacer_3;
    QDoubleSpinBox *doubleSpinBox_price;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *lineEdit_photo_directory;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_directory;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *okButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *ProductData)
    {
        if (ProductData->objectName().isEmpty())
            ProductData->setObjectName(QString::fromUtf8("ProductData"));
        ProductData->resize(429, 257);
        horizontalLayout_4 = new QHBoxLayout(ProductData);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(ProductData);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_5);

        label_2 = new QLabel(ProductData);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_6);

        label_3 = new QLabel(ProductData);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout->addWidget(label_3);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_7);

        label_4 = new QLabel(ProductData);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout->addWidget(label_4);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lineEdit_name = new QLineEdit(ProductData);
        lineEdit_name->setObjectName(QString::fromUtf8("lineEdit_name"));

        verticalLayout_2->addWidget(lineEdit_name);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        lineEdit_category = new QLineEdit(ProductData);
        lineEdit_category->setObjectName(QString::fromUtf8("lineEdit_category"));

        verticalLayout_2->addWidget(lineEdit_category);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        doubleSpinBox_price = new QDoubleSpinBox(ProductData);
        doubleSpinBox_price->setObjectName(QString::fromUtf8("doubleSpinBox_price"));
        doubleSpinBox_price->setMinimum(0.500000000000000);
        doubleSpinBox_price->setMaximum(9999.989999999999782);

        verticalLayout_2->addWidget(doubleSpinBox_price);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        lineEdit_photo_directory = new QLineEdit(ProductData);
        lineEdit_photo_directory->setObjectName(QString::fromUtf8("lineEdit_photo_directory"));

        verticalLayout_2->addWidget(lineEdit_photo_directory);


        horizontalLayout->addLayout(verticalLayout_2);


        horizontalLayout_2->addLayout(horizontalLayout);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        pushButton_directory = new QPushButton(ProductData);
        pushButton_directory->setObjectName(QString::fromUtf8("pushButton_directory"));

        verticalLayout_3->addWidget(pushButton_directory);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        okButton = new QPushButton(ProductData);
        okButton->setObjectName(QString::fromUtf8("okButton"));

        horizontalLayout_3->addWidget(okButton);

        cancelButton = new QPushButton(ProductData);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout_3->addWidget(cancelButton);


        verticalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_4->addLayout(verticalLayout_4);


        retranslateUi(ProductData);
        QObject::connect(okButton, SIGNAL(clicked()), ProductData, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), ProductData, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProductData);
    } // setupUi

    void retranslateUi(QDialog *ProductData)
    {
        ProductData->setWindowTitle(QApplication::translate("ProductData", "Dialog", nullptr));
        label->setText(QApplication::translate("ProductData", "Name:", nullptr));
        label_2->setText(QApplication::translate("ProductData", "Category:", nullptr));
        label_3->setText(QApplication::translate("ProductData", "Price:", nullptr));
        label_4->setText(QApplication::translate("ProductData", "Photo:", nullptr));
        pushButton_directory->setText(QApplication::translate("ProductData", "...", nullptr));
        okButton->setText(QApplication::translate("ProductData", "OK", nullptr));
        cancelButton->setText(QApplication::translate("ProductData", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProductData: public Ui_ProductData {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRODUCTDATA_H
