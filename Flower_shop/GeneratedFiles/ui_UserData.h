/********************************************************************************
** Form generated from reading UI file 'UserData.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERDATA_H
#define UI_USERDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_UserData
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_2;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_3;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_4;
    QSpacerItem *verticalSpacer;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *lineEdit_login;
    QSpacerItem *verticalSpacer_5;
    QLineEdit *lineEdit_password;
    QSpacerItem *verticalSpacer_6;
    QLineEdit *lineEdit_email;
    QSpacerItem *verticalSpacer_7;
    QLineEdit *lineEdit_question;
    QSpacerItem *verticalSpacer_8;
    QLineEdit *lineEdit_answer;
    QPushButton *okButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *UserData)
    {
        if (UserData->objectName().isEmpty())
            UserData->setObjectName(QString::fromUtf8("UserData"));
        UserData->resize(400, 370);
        verticalLayout_3 = new QVBoxLayout(UserData);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(UserData);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        label_2 = new QLabel(UserData);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        label_3 = new QLabel(UserData);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout->addWidget(label_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        label_4 = new QLabel(UserData);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout->addWidget(label_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        label_5 = new QLabel(UserData);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout->addWidget(label_5);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lineEdit_login = new QLineEdit(UserData);
        lineEdit_login->setObjectName(QString::fromUtf8("lineEdit_login"));

        verticalLayout_2->addWidget(lineEdit_login);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_5);

        lineEdit_password = new QLineEdit(UserData);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));

        verticalLayout_2->addWidget(lineEdit_password);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_6);

        lineEdit_email = new QLineEdit(UserData);
        lineEdit_email->setObjectName(QString::fromUtf8("lineEdit_email"));

        verticalLayout_2->addWidget(lineEdit_email);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_7);

        lineEdit_question = new QLineEdit(UserData);
        lineEdit_question->setObjectName(QString::fromUtf8("lineEdit_question"));

        verticalLayout_2->addWidget(lineEdit_question);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_8);

        lineEdit_answer = new QLineEdit(UserData);
        lineEdit_answer->setObjectName(QString::fromUtf8("lineEdit_answer"));

        verticalLayout_2->addWidget(lineEdit_answer);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout);

        okButton = new QPushButton(UserData);
        okButton->setObjectName(QString::fromUtf8("okButton"));

        verticalLayout_3->addWidget(okButton);

        cancelButton = new QPushButton(UserData);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        verticalLayout_3->addWidget(cancelButton);


        retranslateUi(UserData);
        QObject::connect(okButton, SIGNAL(clicked()), UserData, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), UserData, SLOT(reject()));

        QMetaObject::connectSlotsByName(UserData);
    } // setupUi

    void retranslateUi(QDialog *UserData)
    {
        UserData->setWindowTitle(QApplication::translate("UserData", "Dialog", nullptr));
        label->setText(QApplication::translate("UserData", "Login:", nullptr));
        label_2->setText(QApplication::translate("UserData", "Password:", nullptr));
        label_3->setText(QApplication::translate("UserData", "Email:", nullptr));
        label_4->setText(QApplication::translate("UserData", "Password reset question:", nullptr));
        label_5->setText(QApplication::translate("UserData", "Answer:", nullptr));
        okButton->setText(QApplication::translate("UserData", "OK", nullptr));
        cancelButton->setText(QApplication::translate("UserData", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserData: public Ui_UserData {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERDATA_H
