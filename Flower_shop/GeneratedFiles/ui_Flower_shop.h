/********************************************************************************
** Form generated from reading UI file 'Flower_shop.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLOWER_SHOP_H
#define UI_FLOWER_SHOP_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Flower_shopClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QListWidget *list_categories;
    QListWidget *list_products;
    QLabel *label;
    QLabel *label_2;
    QFrame *line;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit_search;
    QPushButton *pushButton_addToCart;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QSpinBox *spinBox_amount;
    QWidget *tab_2;
    QListWidget *listWidget_cart;
    QLabel *label_5;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_finalize_order;
    QPushButton *pushButton_delete_cart;
    QLabel *label_cart_value;
    QWidget *tab_3;
    QListWidget *listWidget_adress;
    QLabel *label_3;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_add_adress;
    QPushButton *pushButton_edit_adress;
    QPushButton *pushButton_set_active_adress;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_edit_user;
    QPushButton *pushButton_delete_account;
    QWidget *tab_4;
    QListWidget *listWidget_orders;
    QLabel *label_6;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *Flower_shopClass)
    {
        if (Flower_shopClass->objectName().isEmpty())
            Flower_shopClass->setObjectName(QString::fromUtf8("Flower_shopClass"));
        Flower_shopClass->resize(824, 586);
        QFont font;
        font.setFamily(QString::fromUtf8("Segoe UI Symbol"));
        Flower_shopClass->setFont(font);
        Flower_shopClass->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        centralWidget = new QWidget(Flower_shopClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        list_categories = new QListWidget(tab);
        list_categories->setObjectName(QString::fromUtf8("list_categories"));
        list_categories->setGeometry(QRect(0, 30, 81, 471));
        list_products = new QListWidget(tab);
        list_products->setObjectName(QString::fromUtf8("list_products"));
        list_products->setGeometry(QRect(100, 30, 691, 441));
        list_products->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        list_products->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 10, 71, 16));
        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(100, 10, 61, 16));
        line = new QFrame(tab);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(80, 30, 20, 471));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        layoutWidget = new QWidget(tab);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(600, 0, 191, 26));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        lineEdit_search = new QLineEdit(layoutWidget);
        lineEdit_search->setObjectName(QString::fromUtf8("lineEdit_search"));

        horizontalLayout_2->addWidget(lineEdit_search);

        pushButton_addToCart = new QPushButton(tab);
        pushButton_addToCart->setObjectName(QString::fromUtf8("pushButton_addToCart"));
        pushButton_addToCart->setGeometry(QRect(619, 479, 171, 28));
        layoutWidget1 = new QWidget(tab);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(520, 480, 99, 26));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_3->addWidget(label_4);

        spinBox_amount = new QSpinBox(layoutWidget1);
        spinBox_amount->setObjectName(QString::fromUtf8("spinBox_amount"));
        spinBox_amount->setMinimum(1);

        horizontalLayout_3->addWidget(spinBox_amount);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        listWidget_cart = new QListWidget(tab_2);
        listWidget_cart->setObjectName(QString::fromUtf8("listWidget_cart"));
        listWidget_cart->setGeometry(QRect(320, 50, 451, 441));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(320, 20, 81, 16));
        layoutWidget2 = new QWidget(tab_2);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(60, 50, 171, 131));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton_finalize_order = new QPushButton(layoutWidget2);
        pushButton_finalize_order->setObjectName(QString::fromUtf8("pushButton_finalize_order"));

        verticalLayout_2->addWidget(pushButton_finalize_order);

        pushButton_delete_cart = new QPushButton(layoutWidget2);
        pushButton_delete_cart->setObjectName(QString::fromUtf8("pushButton_delete_cart"));

        verticalLayout_2->addWidget(pushButton_delete_cart);

        label_cart_value = new QLabel(tab_2);
        label_cart_value->setObjectName(QString::fromUtf8("label_cart_value"));
        label_cart_value->setGeometry(QRect(70, 270, 151, 16));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        listWidget_adress = new QListWidget(tab_3);
        listWidget_adress->setObjectName(QString::fromUtf8("listWidget_adress"));
        listWidget_adress->setGeometry(QRect(350, 50, 411, 391));
        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(350, 20, 131, 16));
        layoutWidget3 = new QWidget(tab_3);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(70, 50, 201, 391));
        verticalLayout = new QVBoxLayout(layoutWidget3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_add_adress = new QPushButton(layoutWidget3);
        pushButton_add_adress->setObjectName(QString::fromUtf8("pushButton_add_adress"));

        verticalLayout->addWidget(pushButton_add_adress);

        pushButton_edit_adress = new QPushButton(layoutWidget3);
        pushButton_edit_adress->setObjectName(QString::fromUtf8("pushButton_edit_adress"));

        verticalLayout->addWidget(pushButton_edit_adress);

        pushButton_set_active_adress = new QPushButton(layoutWidget3);
        pushButton_set_active_adress->setObjectName(QString::fromUtf8("pushButton_set_active_adress"));

        verticalLayout->addWidget(pushButton_set_active_adress);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        pushButton_edit_user = new QPushButton(layoutWidget3);
        pushButton_edit_user->setObjectName(QString::fromUtf8("pushButton_edit_user"));

        verticalLayout->addWidget(pushButton_edit_user);

        pushButton_delete_account = new QPushButton(layoutWidget3);
        pushButton_delete_account->setObjectName(QString::fromUtf8("pushButton_delete_account"));

        verticalLayout->addWidget(pushButton_delete_account);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        listWidget_orders = new QListWidget(tab_4);
        listWidget_orders->setObjectName(QString::fromUtf8("listWidget_orders"));
        listWidget_orders->setGeometry(QRect(30, 50, 731, 421));
        listWidget_orders->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        listWidget_orders->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        label_6 = new QLabel(tab_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(30, 20, 81, 16));
        tabWidget->addTab(tab_4, QString());

        horizontalLayout->addWidget(tabWidget);

        Flower_shopClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Flower_shopClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 824, 26));
        Flower_shopClass->setMenuBar(menuBar);

        retranslateUi(Flower_shopClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Flower_shopClass);
    } // setupUi

    void retranslateUi(QMainWindow *Flower_shopClass)
    {
        Flower_shopClass->setWindowTitle(QApplication::translate("Flower_shopClass", "Flower_shop", nullptr));
        label->setText(QApplication::translate("Flower_shopClass", "Categories:", nullptr));
        label_2->setText(QApplication::translate("Flower_shopClass", "Products:", nullptr));
        pushButton_addToCart->setText(QApplication::translate("Flower_shopClass", "Add to Cart", nullptr));
        label_4->setText(QApplication::translate("Flower_shopClass", "Amount:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Flower_shopClass", "Shop", nullptr));
        label_5->setText(QApplication::translate("Flower_shopClass", "Your cart:", nullptr));
        pushButton_finalize_order->setText(QApplication::translate("Flower_shopClass", "Finalize order", nullptr));
        pushButton_delete_cart->setText(QApplication::translate("Flower_shopClass", "Delete from cart", nullptr));
        label_cart_value->setText(QApplication::translate("Flower_shopClass", "Cart value: 0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Flower_shopClass", "Cart", nullptr));
        label_3->setText(QApplication::translate("Flower_shopClass", "Your adresses:", nullptr));
        pushButton_add_adress->setText(QApplication::translate("Flower_shopClass", "Add adress", nullptr));
        pushButton_edit_adress->setText(QApplication::translate("Flower_shopClass", "Edit Adress", nullptr));
        pushButton_set_active_adress->setText(QApplication::translate("Flower_shopClass", "Set active adress", nullptr));
        pushButton_edit_user->setText(QApplication::translate("Flower_shopClass", "Edit account data", nullptr));
        pushButton_delete_account->setText(QApplication::translate("Flower_shopClass", "Delete account", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("Flower_shopClass", "Account", nullptr));
        label_6->setText(QApplication::translate("Flower_shopClass", "Your orders:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("Flower_shopClass", "Orders", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Flower_shopClass: public Ui_Flower_shopClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLOWER_SHOP_H
