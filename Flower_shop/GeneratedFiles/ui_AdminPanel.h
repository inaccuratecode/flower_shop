/********************************************************************************
** Form generated from reading UI file 'AdminPanel.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINPANEL_H
#define UI_ADMINPANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AdminPanel
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QListWidget *listWidget_Users;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_addUser;
    QPushButton *pushButton_clearUsers;
    QPushButton *pushButton_change_usr_typ;
    QWidget *tab_2;
    QListWidget *listWidget_orders;
    QPushButton *pushButton_orderstatus;
    QComboBox *comboBox_order_status;
    QLabel *label;
    QComboBox *comboBox_filter;
    QWidget *tab_3;
    QPushButton *pushButton_addProduct;
    QPushButton *pushButton_EditProduct;
    QListWidget *listWidget_products;
    QPushButton *pushButton_supply;
    QSpinBox *spinBox_supply_amount;
    QPushButton *pushButton_sale_status;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *AdminPanel)
    {
        if (AdminPanel->objectName().isEmpty())
            AdminPanel->setObjectName(QString::fromUtf8("AdminPanel"));
        AdminPanel->resize(483, 400);
        centralwidget = new QWidget(AdminPanel);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        listWidget_Users = new QListWidget(tab);
        listWidget_Users->setObjectName(QString::fromUtf8("listWidget_Users"));
        listWidget_Users->setGeometry(QRect(160, 20, 281, 271));
        listWidget_Users->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        listWidget_Users->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        layoutWidget = new QWidget(tab);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 69, 111, 161));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_addUser = new QPushButton(layoutWidget);
        pushButton_addUser->setObjectName(QString::fromUtf8("pushButton_addUser"));

        verticalLayout->addWidget(pushButton_addUser);

        pushButton_clearUsers = new QPushButton(layoutWidget);
        pushButton_clearUsers->setObjectName(QString::fromUtf8("pushButton_clearUsers"));

        verticalLayout->addWidget(pushButton_clearUsers);

        pushButton_change_usr_typ = new QPushButton(layoutWidget);
        pushButton_change_usr_typ->setObjectName(QString::fromUtf8("pushButton_change_usr_typ"));

        verticalLayout->addWidget(pushButton_change_usr_typ);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        listWidget_orders = new QListWidget(tab_2);
        listWidget_orders->setObjectName(QString::fromUtf8("listWidget_orders"));
        listWidget_orders->setGeometry(QRect(160, 20, 281, 271));
        listWidget_orders->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        listWidget_orders->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        pushButton_orderstatus = new QPushButton(tab_2);
        pushButton_orderstatus->setObjectName(QString::fromUtf8("pushButton_orderstatus"));
        pushButton_orderstatus->setGeometry(QRect(10, 40, 131, 28));
        comboBox_order_status = new QComboBox(tab_2);
        comboBox_order_status->addItem(QString());
        comboBox_order_status->addItem(QString());
        comboBox_order_status->addItem(QString());
        comboBox_order_status->addItem(QString());
        comboBox_order_status->setObjectName(QString::fromUtf8("comboBox_order_status"));
        comboBox_order_status->setGeometry(QRect(10, 80, 131, 21));
        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 230, 101, 16));
        comboBox_filter = new QComboBox(tab_2);
        comboBox_filter->addItem(QString());
        comboBox_filter->addItem(QString());
        comboBox_filter->addItem(QString());
        comboBox_filter->addItem(QString());
        comboBox_filter->addItem(QString());
        comboBox_filter->addItem(QString());
        comboBox_filter->setObjectName(QString::fromUtf8("comboBox_filter"));
        comboBox_filter->setGeometry(QRect(10, 260, 131, 21));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        pushButton_addProduct = new QPushButton(tab_3);
        pushButton_addProduct->setObjectName(QString::fromUtf8("pushButton_addProduct"));
        pushButton_addProduct->setGeometry(QRect(30, 70, 181, 28));
        pushButton_EditProduct = new QPushButton(tab_3);
        pushButton_EditProduct->setObjectName(QString::fromUtf8("pushButton_EditProduct"));
        pushButton_EditProduct->setGeometry(QRect(30, 120, 181, 28));
        listWidget_products = new QListWidget(tab_3);
        listWidget_products->setObjectName(QString::fromUtf8("listWidget_products"));
        listWidget_products->setGeometry(QRect(230, 20, 211, 271));
        pushButton_supply = new QPushButton(tab_3);
        pushButton_supply->setObjectName(QString::fromUtf8("pushButton_supply"));
        pushButton_supply->setGeometry(QRect(30, 160, 101, 28));
        spinBox_supply_amount = new QSpinBox(tab_3);
        spinBox_supply_amount->setObjectName(QString::fromUtf8("spinBox_supply_amount"));
        spinBox_supply_amount->setGeometry(QRect(140, 163, 71, 22));
        spinBox_supply_amount->setMinimum(-999);
        spinBox_supply_amount->setMaximum(999);
        pushButton_sale_status = new QPushButton(tab_3);
        pushButton_sale_status->setObjectName(QString::fromUtf8("pushButton_sale_status"));
        pushButton_sale_status->setGeometry(QRect(30, 200, 181, 28));
        tabWidget->addTab(tab_3, QString());

        horizontalLayout->addWidget(tabWidget);

        AdminPanel->setCentralWidget(centralwidget);
        menubar = new QMenuBar(AdminPanel);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 483, 26));
        AdminPanel->setMenuBar(menubar);
        statusbar = new QStatusBar(AdminPanel);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        AdminPanel->setStatusBar(statusbar);

        retranslateUi(AdminPanel);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(AdminPanel);
    } // setupUi

    void retranslateUi(QMainWindow *AdminPanel)
    {
        AdminPanel->setWindowTitle(QApplication::translate("AdminPanel", "MainWindow", nullptr));
        pushButton_addUser->setText(QApplication::translate("AdminPanel", "Add User", nullptr));
        pushButton_clearUsers->setText(QApplication::translate("AdminPanel", "Clear Users", nullptr));
        pushButton_change_usr_typ->setText(QApplication::translate("AdminPanel", "Change user type", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("AdminPanel", "Home", nullptr));
        pushButton_orderstatus->setText(QApplication::translate("AdminPanel", "Change order status", nullptr));
        comboBox_order_status->setItemText(0, QApplication::translate("AdminPanel", "ordered", nullptr));
        comboBox_order_status->setItemText(1, QApplication::translate("AdminPanel", "in progress", nullptr));
        comboBox_order_status->setItemText(2, QApplication::translate("AdminPanel", "finished", nullptr));
        comboBox_order_status->setItemText(3, QApplication::translate("AdminPanel", "returned", nullptr));

        label->setText(QApplication::translate("AdminPanel", "Show by type:", nullptr));
        comboBox_filter->setItemText(0, QApplication::translate("AdminPanel", "all", nullptr));
        comboBox_filter->setItemText(1, QApplication::translate("AdminPanel", "ordered", nullptr));
        comboBox_filter->setItemText(2, QApplication::translate("AdminPanel", "in progress", nullptr));
        comboBox_filter->setItemText(3, QApplication::translate("AdminPanel", "finished", nullptr));
        comboBox_filter->setItemText(4, QApplication::translate("AdminPanel", "returned", nullptr));
        comboBox_filter->setItemText(5, QApplication::translate("AdminPanel", "canceled", nullptr));

        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AdminPanel", "Orders", nullptr));
        pushButton_addProduct->setText(QApplication::translate("AdminPanel", "Add Product", nullptr));
        pushButton_EditProduct->setText(QApplication::translate("AdminPanel", "Edit Product", nullptr));
        pushButton_supply->setText(QApplication::translate("AdminPanel", "Product supply", nullptr));
        pushButton_sale_status->setText(QApplication::translate("AdminPanel", "Change sale status", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("AdminPanel", "Products", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AdminPanel: public Ui_AdminPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINPANEL_H
