/********************************************************************************
** Form generated from reading UI file 'LoginWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab_login;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_login_2;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_password_2;
    QSpacerItem *verticalSpacer_4;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *lineEdit_login_2;
    QSpacerItem *verticalSpacer;
    QLineEdit *lineEdit_password_2;
    QSpacerItem *verticalSpacer_3;
    QFrame *line;
    QPushButton *LoginButton_2;
    QWidget *tab_reset;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_17;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_login_reset;
    QVBoxLayout *verticalLayout_12;
    QLineEdit *lineEdit_login_reset;
    QPushButton *pushButton_resetpassword_2;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_login_reset_question;
    QLabel *label_login_reset_answer;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_question;
    QLineEdit *lineEdit_login_reset_answer;
    QPushButton *pushButton_resetpassword_answer;
    QWidget *tab_recovery;
    QVBoxLayout *verticalLayout_19;
    QVBoxLayout *verticalLayout_18;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_login_reset_3;
    QLineEdit *lineEdit_reset_login_2;
    QPushButton *pushButton_recovery_2;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(364, 234);
        gridLayout = new QGridLayout(Form);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetMinimumSize);
        tabWidget = new QTabWidget(Form);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_login = new QWidget();
        tab_login->setObjectName(QString::fromUtf8("tab_login"));
        horizontalLayout_2 = new QHBoxLayout(tab_login);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_login_2 = new QLabel(tab_login);
        label_login_2->setObjectName(QString::fromUtf8("label_login_2"));

        verticalLayout_6->addWidget(label_login_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_2);

        label_password_2 = new QLabel(tab_login);
        label_password_2->setObjectName(QString::fromUtf8("label_password_2"));

        verticalLayout_6->addWidget(label_password_2);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_4);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        lineEdit_login_2 = new QLineEdit(tab_login);
        lineEdit_login_2->setObjectName(QString::fromUtf8("lineEdit_login_2"));

        verticalLayout_5->addWidget(lineEdit_login_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        lineEdit_password_2 = new QLineEdit(tab_login);
        lineEdit_password_2->setObjectName(QString::fromUtf8("lineEdit_password_2"));
        lineEdit_password_2->setEchoMode(QLineEdit::Password);

        verticalLayout_5->addWidget(lineEdit_password_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_3);


        horizontalLayout_3->addLayout(verticalLayout_5);


        horizontalLayout->addLayout(horizontalLayout_3);

        line = new QFrame(tab_login);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);


        verticalLayout_2->addLayout(horizontalLayout);

        LoginButton_2 = new QPushButton(tab_login);
        LoginButton_2->setObjectName(QString::fromUtf8("LoginButton_2"));

        verticalLayout_2->addWidget(LoginButton_2);


        horizontalLayout_2->addLayout(verticalLayout_2);

        tabWidget->addTab(tab_login, QString());
        tab_reset = new QWidget();
        tab_reset->setObjectName(QString::fromUtf8("tab_reset"));
        horizontalLayout_8 = new QHBoxLayout(tab_reset);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_login_reset = new QLabel(tab_reset);
        label_login_reset->setObjectName(QString::fromUtf8("label_login_reset"));

        verticalLayout_13->addWidget(label_login_reset);


        horizontalLayout_6->addLayout(verticalLayout_13);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        lineEdit_login_reset = new QLineEdit(tab_reset);
        lineEdit_login_reset->setObjectName(QString::fromUtf8("lineEdit_login_reset"));

        verticalLayout_12->addWidget(lineEdit_login_reset);


        horizontalLayout_6->addLayout(verticalLayout_12);


        verticalLayout_14->addLayout(horizontalLayout_6);

        pushButton_resetpassword_2 = new QPushButton(tab_reset);
        pushButton_resetpassword_2->setObjectName(QString::fromUtf8("pushButton_resetpassword_2"));

        verticalLayout_14->addWidget(pushButton_resetpassword_2);


        verticalLayout_17->addLayout(verticalLayout_14);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        label_login_reset_question = new QLabel(tab_reset);
        label_login_reset_question->setObjectName(QString::fromUtf8("label_login_reset_question"));

        verticalLayout_15->addWidget(label_login_reset_question);

        label_login_reset_answer = new QLabel(tab_reset);
        label_login_reset_answer->setObjectName(QString::fromUtf8("label_login_reset_answer"));

        verticalLayout_15->addWidget(label_login_reset_answer);


        horizontalLayout_7->addLayout(verticalLayout_15);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_question = new QLabel(tab_reset);
        label_question->setObjectName(QString::fromUtf8("label_question"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_question->setFont(font);

        verticalLayout_16->addWidget(label_question);

        lineEdit_login_reset_answer = new QLineEdit(tab_reset);
        lineEdit_login_reset_answer->setObjectName(QString::fromUtf8("lineEdit_login_reset_answer"));
        lineEdit_login_reset_answer->setEnabled(false);

        verticalLayout_16->addWidget(lineEdit_login_reset_answer);


        horizontalLayout_7->addLayout(verticalLayout_16);


        verticalLayout_17->addLayout(horizontalLayout_7);

        pushButton_resetpassword_answer = new QPushButton(tab_reset);
        pushButton_resetpassword_answer->setObjectName(QString::fromUtf8("pushButton_resetpassword_answer"));

        verticalLayout_17->addWidget(pushButton_resetpassword_answer);


        horizontalLayout_8->addLayout(verticalLayout_17);

        tabWidget->addTab(tab_reset, QString());
        tab_recovery = new QWidget();
        tab_recovery->setObjectName(QString::fromUtf8("tab_recovery"));
        verticalLayout_19 = new QVBoxLayout(tab_recovery);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_login_reset_3 = new QLabel(tab_recovery);
        label_login_reset_3->setObjectName(QString::fromUtf8("label_login_reset_3"));

        horizontalLayout_9->addWidget(label_login_reset_3);

        lineEdit_reset_login_2 = new QLineEdit(tab_recovery);
        lineEdit_reset_login_2->setObjectName(QString::fromUtf8("lineEdit_reset_login_2"));

        horizontalLayout_9->addWidget(lineEdit_reset_login_2);


        verticalLayout_18->addLayout(horizontalLayout_9);

        pushButton_recovery_2 = new QPushButton(tab_recovery);
        pushButton_recovery_2->setObjectName(QString::fromUtf8("pushButton_recovery_2"));

        verticalLayout_18->addWidget(pushButton_recovery_2);


        verticalLayout_19->addLayout(verticalLayout_18);

        tabWidget->addTab(tab_recovery, QString());

        gridLayout->addWidget(tabWidget, 1, 0, 1, 1);


        retranslateUi(Form);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", nullptr));
#ifndef QT_NO_WHATSTHIS
        Form->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        label_login_2->setText(QApplication::translate("Form", "Login", nullptr));
        label_password_2->setText(QApplication::translate("Form", "Password", nullptr));
        LoginButton_2->setText(QApplication::translate("Form", "Login", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_login), QApplication::translate("Form", "Login", nullptr));
        label_login_reset->setText(QApplication::translate("Form", "Login", nullptr));
        pushButton_resetpassword_2->setText(QApplication::translate("Form", "Get reset question", nullptr));
        label_login_reset_question->setText(QApplication::translate("Form", "Question:", nullptr));
        label_login_reset_answer->setText(QApplication::translate("Form", "Answer:", nullptr));
        label_question->setText(QApplication::translate("Form", "Question will appear here", nullptr));
        pushButton_resetpassword_answer->setText(QApplication::translate("Form", "Reset Password", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_reset), QApplication::translate("Form", "Reset Password", nullptr));
        label_login_reset_3->setText(QApplication::translate("Form", "Login::", nullptr));
        pushButton_recovery_2->setText(QApplication::translate("Form", "Recover Account", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_recovery), QApplication::translate("Form", "Password Recovery", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
