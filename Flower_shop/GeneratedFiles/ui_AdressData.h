/********************************************************************************
** Form generated from reading UI file 'AdressData.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADRESSDATA_H
#define UI_ADRESSDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AdressData
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit_street;
    QLineEdit *lineEdit_city;
    QLineEdit *lineEdit_postcode;
    QLineEdit *lineEdit_phone;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *okButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *AdressData)
    {
        if (AdressData->objectName().isEmpty())
            AdressData->setObjectName(QString::fromUtf8("AdressData"));
        AdressData->resize(400, 180);
        verticalLayout_3 = new QVBoxLayout(AdressData);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(AdressData);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(AdressData);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(AdressData);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_4 = new QLabel(AdressData);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_2->addWidget(label_4);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lineEdit_street = new QLineEdit(AdressData);
        lineEdit_street->setObjectName(QString::fromUtf8("lineEdit_street"));

        verticalLayout->addWidget(lineEdit_street);

        lineEdit_city = new QLineEdit(AdressData);
        lineEdit_city->setObjectName(QString::fromUtf8("lineEdit_city"));

        verticalLayout->addWidget(lineEdit_city);

        lineEdit_postcode = new QLineEdit(AdressData);
        lineEdit_postcode->setObjectName(QString::fromUtf8("lineEdit_postcode"));

        verticalLayout->addWidget(lineEdit_postcode);

        lineEdit_phone = new QLineEdit(AdressData);
        lineEdit_phone->setObjectName(QString::fromUtf8("lineEdit_phone"));

        verticalLayout->addWidget(lineEdit_phone);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        okButton = new QPushButton(AdressData);
        okButton->setObjectName(QString::fromUtf8("okButton"));

        horizontalLayout_2->addWidget(okButton);

        cancelButton = new QPushButton(AdressData);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout_2->addWidget(cancelButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        retranslateUi(AdressData);
        QObject::connect(okButton, SIGNAL(clicked()), AdressData, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), AdressData, SLOT(reject()));

        QMetaObject::connectSlotsByName(AdressData);
    } // setupUi

    void retranslateUi(QDialog *AdressData)
    {
        AdressData->setWindowTitle(QApplication::translate("AdressData", "Dialog", nullptr));
        label->setText(QApplication::translate("AdressData", "Street:", nullptr));
        label_2->setText(QApplication::translate("AdressData", "City:", nullptr));
        label_3->setText(QApplication::translate("AdressData", "Postcode:", nullptr));
        label_4->setText(QApplication::translate("AdressData", "Phone:", nullptr));
        lineEdit_postcode->setPlaceholderText(QApplication::translate("AdressData", "Format: 33222", nullptr));
        lineEdit_phone->setPlaceholderText(QApplication::translate("AdressData", "Format: 123456789", nullptr));
        okButton->setText(QApplication::translate("AdressData", "OK", nullptr));
        cancelButton->setText(QApplication::translate("AdressData", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AdressData: public Ui_AdressData {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADRESSDATA_H
