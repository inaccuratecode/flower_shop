#include "stdafx.h"
#include "Login_window.h"
#include "Window_Reset_Password.h"


Login_window::Login_window(QDialog *parent, Users* users_data)
	: QDialog(parent)
{
	this->users_data = users_data;
	this->setWindowTitle("Flower shop");
	ui.setupUi(this);
	setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
	this->setWindowTitle("Login");
}

void Login_window::on_LoginButton_2_clicked() {
	QString tmp_login, tmp_password;

	tmp_login = ui.lineEdit_login_2->text();
	tmp_password = ui.lineEdit_password_2->text();

	ui.lineEdit_login_2->clear();
	ui.lineEdit_password_2->clear();

	if (tmp_login.length() == 0 || tmp_password.length() == 0) {
		QMessageBox::warning(NULL, "Error", "No login or password");
		return;
	}

	if (users_data != NULL) {
		//Callig method of login authentication
		users_data->login(tmp_login, tmp_password);

		if (users_data->login_status()) {
			QMessageBox::information(NULL, "Success", "You are logged in!");
			accept();
			return;
		}
		else
			return;
	}
	else {
		QMessageBox::critical(NULL, "ERROR", "Can`t access users data!");
		return;
	}

}

void Login_window::closeEvent(QCloseEvent * e)
{
	QMessageBox::StandardButton reply = QMessageBox::question(NULL, "Exit", "Do you want to exit application?", QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes) {
		e->accept();
		return;
	}
	else
		e->ignore();

}

//Getting reset password question from database
void Login_window::on_pushButton_resetpassword_2_clicked()
{
	QString tmp_login, question;
	tmp_login = ui.lineEdit_login_reset->text();

	question = users_data->get_question(tmp_login);
	if (question.length() == 0 || tmp_login.length() == 0) {
		QMessageBox::critical(NULL, "ERROR", "Can`t get reset question data");
		return;
	}
	else {
		ui.lineEdit_login_reset->setEnabled(false);
		ui.label_question->setText(question);
		ui.lineEdit_login_reset_answer->setEnabled(true);
	}

	return;
}

//Authentication of reset question answer
void Login_window::on_pushButton_resetpassword_answer_clicked()
{
	QString answer, question, login;
	bool reset_confirmation;
	answer = ui.lineEdit_login_reset_answer->text();
	question = ui.label_question->text();
	login = ui.lineEdit_login_reset->text();
	ui.lineEdit_login_reset_answer->clear();

	if (answer.length() == 0) {
		QMessageBox::critical(NULL, "ERROR", "Can`t get reset answer data");
		return;
	}
	else {
		reset_confirmation = users_data->compare_answer_questrion(login, question, answer);
		if (reset_confirmation) {
			Window_Reset_Password reset_window(nullptr, users_data, login);
			reset_window.setModal(true);
			reset_window.show();
			reset_window.exec();
		}
		else {
			QMessageBox::warning(NULL, "ERROR", "Wrong answer");
			return;
		}
	}
	ui.lineEdit_login_reset_answer->setEnabled(false);
	ui.lineEdit_login_reset->setEnabled(true);
	ui.lineEdit_login_reset->clear();
	ui.label_question->setText("Question will appear here");
}

//Recovering deleted user account
void Login_window::on_pushButton_recovery_2_clicked()
{
	QString tmp_login;
	tmp_login = ui.lineEdit_reset_login_2->text();
	users_data->set_user_delete(tmp_login, false);

	ui.lineEdit_reset_login_2->clear();
	QMessageBox::information(NULL, "Recovery", "Recovery complete");
}
