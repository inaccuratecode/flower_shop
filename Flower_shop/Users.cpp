#include "stdafx.h"
#include "Users.h"
#include <QMessageBox>
#include <Cypher_Decypher.h>

//Main constructor
Users::Users(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query)
{
	this->database = database;
	this->query = query;
	this->extra_query = extra_query;
}

//Login authentication
bool Users::login(QString login, QString password)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	if (status) {
		QMessageBox::warning(NULL, "ERROR", "FATAL ERROR. There is active user. Contact administrator!");
		return false;
	}

	int pos;
	//encrypting given password
	password = QString::fromStdString(prepare_encrypt<string>(password.toStdString()));
		
	query->clear();
	//Searching user with given password and login
		query->prepare("SELECT `login`, `type`, `deleted`, `active_adress` FROM `users` WHERE login=? AND us_password=?");
		query->bindValue(0, login);
		query->bindValue(1, password);
		if (!query->exec()) {
			QMessageBox::warning(NULL, "ERROR", "Failed to get account data from database");
			return "Failed to get account data from database";
		}

		if (query->next()) {

			//Checking if account is not deleted
			if (query->value(2).toInt() == 1) {
				QMessageBox::information(NULL, "Account is deleted", "Can`t login to deleted account. Try recover your account");
				return false;
			}

			active_user_login = query->value(0).toString();
			active_user_type = query->value(1).toString().toStdString().at(0);
			if (query->value(3)!=NULL)
				active_user_adress = query->value(3).toInt();
			status = true;
		}
		else {
			QMessageBox::warning(NULL, "Unable to login", "Wrong login or password");
			return false;
		}

	return status;
}

//Login status getter
bool Users::login_status()
{
	return status;
}

//User type getter
char Users::get_user_type()
{
	return active_user_type;
}

bool Users::add_user(QString login, QString password, QString email, QString question, QString answer)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Checking if selected login is available 
	query->prepare("SELECT login FROM users where login=?");
	query->bindValue(0, login);
	query->exec();
	if (query->next()) {
		QMessageBox::warning(NULL, "ERROR", "User with such login already exists. \nIf you deleted your data before, try recover your data on login screen.");
		return false;
	}

	//Encrypting password
	password = QString::fromStdString(prepare_encrypt<std::string>(password.toStdString()));
	if (password.size() >= 3)
		if (password[0] == '#' && password[1] == '#' && password[2] == '#') {
			std::string err = "Error while encrypting password. " + password.toStdString();
			QMessageBox::warning(NULL, "ERROR", QString::fromStdString(err));
			return false;
		}

	//Encrypting reset question and answer
	question = QString::fromStdString(prepare_encrypt<std::string>(question.toStdString()));
	answer = QString::fromStdString(prepare_encrypt<std::string>(answer.toStdString()));

	//Updating database
	query->prepare("INSERT INTO `users`(`login`, `us_password`, `email`, `question`, `answer`, `type`) VALUES (?,?,?,?,?,?)");
	query->bindValue(0, login);
	query->bindValue(1, password);
	query->bindValue(2, email);
	query->bindValue(3, question);
	query->bindValue(4, answer);
	query->bindValue(5, "u");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return "";
	}
	QMessageBox::information(NULL, "Adding user", "New user added!");
	return true;
}

//Getting user data and puting data to list widget
bool Users::get_users(QListWidget * list)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	QListWidgetItem *itm;
	QString data;

	query->prepare("SELECT `login`, `email`, `active_adress`, `type`, `deleted` FROM `users` WHERE 1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		itm = new QListWidgetItem;

		//Generating description
		data = ("Login: " + query->value(0).toString() + "\n" + "Email: " + query->value(1).toString() + "\n" + "Type of account: " + [](QString check)->QString {
			if (check == "a" || check == "s")
				return "administrator";
			else
				return "user";
		}(query->value(3).toString()) + "\n" + "Set to delete: " + [](int check)->QString {
			if (check == 0)
				return "No";
			else
				return "Yes";
		}(query->value(4).toInt()));

		if (query->value(2) != NULL) {
			extra_query->prepare("SELECT `ID`, `street`, `postcode`, `city`, `phone` FROM `adress` WHERE login=? AND ID=?");
			extra_query->bindValue(0, query->value(0).toString());
			extra_query->bindValue(1, query->value(2).toInt());
			if (!extra_query->exec()) {
				QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
				return false;
			}

			extra_query->next();
			data.append("\nActive adress: \n\tAdress ID: " + extra_query->value(0).toString() + "\n\tStreet: " + extra_query->value(1).toString() + "\n\tPostcode: " + extra_query->value(2).toString() + "\n\tCity: " + extra_query->value(3).toString() + "\n\tPhone: " + extra_query->value(4).toString());
		}
		data.append("\n______________________");
		itm->setText(data);
		list->addItem(itm);
	}
	return true;
}

//Adding new postal adress
bool Users::add_adress(QString login, QString street, QString city, int postcode, QString phone)
{
	bool first_adress = false;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->prepare("SELECT `ID` FROM `adress` WHERE `login`=?");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	if (!query->next())
		first_adress = true;

	//Updating database
	query->prepare("INSERT INTO `adress`(`login`, `street`, `postcode`, `city`, `phone`) VALUES (?,?,?,?,?)");
	query->bindValue(0, login);
	query->bindValue(1, street);
	query->bindValue(2, postcode);
	query->bindValue(3, city);
	query->bindValue(4, phone);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	if (first_adress == true) {
		query->prepare("SELECT `ID` FROM `adress` WHERE `login`=?");
		query->bindValue(0, login);
		if (!query->exec()) {
			QMessageBox::critical(NULL, "ERROR", query->lastError().text());
			return false;
		}
		query->next();
		int tmp_ID = query->value(0).toInt();

		query->prepare("UPDATE `users` SET `active_adress`=? WHERE `login`=?");
		query->bindValue(0, tmp_ID);
		query->bindValue(1, login);
		if (!query->exec()) {
			QMessageBox::critical(NULL, "ERROR", query->lastError().text());
			return false;
		}
		active_user_adress = tmp_ID;
	}
	QMessageBox::information(NULL, "Adding adress", "New adress added!");
	return true;
}

std::vector<QString>* Users::get_adress_by_ID(int ID, std::vector<QString>* data)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return data;
	}
	
	query->prepare("SELECT `street`, `postcode`, `city`, `phone` FROM `adress` WHERE `ID`=?");
	query->bindValue(0, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return data;
	}
	query->next();
	data->clear();

	data->push_back(query->value(0).toString());
	data->push_back(query->value(1).toString());
	data->push_back(query->value(2).toString());
	data->push_back(query->value(3).toString());

	return data;
}

bool Users::edit_adress(int ID, QString street, QString city, int postcode, QString phone)
{

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	
	
	query->prepare("UPDATE `adress` SET `street`=?,`postcode`=?,`city`=?,`phone`=? WHERE `ID`=?");
	query->bindValue(0, street);
	query->bindValue(1, postcode);
	query->bindValue(2, city);
	query->bindValue(3, phone);
	query->bindValue(4, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QMessageBox::information(NULL, "Edit", "Adress successfully edited!");
	return true;
}

bool Users::update_active_adress(QString login, int adress_ID)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->prepare("UPDATE `users` SET `active_adress`=? WHERE `login`=?");
	query->bindValue(0, adress_ID);
	query->bindValue(1, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	active_user_adress = adress_ID;
	QMessageBox::information(NULL, "Adress", "Active adress changed!");
	return true;
}

//Getting user adress data and putting it into list widget
bool Users::get_user_adresses(QString login, QListWidget * list)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	QListWidgetItem* itm;
	QString data;
	query->prepare("SELECT `ID`, `street`, `postcode`, `city`, `phone` FROM `adress` WHERE login=?");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	extra_query->prepare("SELECT `active_adress`FROM `users` WHERE `login`=?");
	extra_query->bindValue(0, login);
	if (!extra_query->exec()) {
		QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
		return false;
	}
	extra_query->next();
	list->clear();

	while (query->next()) {
		itm = new QListWidgetItem;
		data = ("Adress ID: " + query->value(0).toString() + "\nStreet: " + query->value(1).toString() + "\nPostcode: " + query->value(2).toString() + "\nCity: " + query->value(3).toString() + "\nPhone: " + query->value(4).toString());
		if (extra_query->value(0).toInt() == query->value(0).toInt())
			data = "\tACTIVE ADRESS:\n" + data;
		itm->setText(data);
		list->addItem(itm);
	}

	return true;
}

//After deleting own account by user, the account is not deleted immediately. First it gets set as deleted, so user can recover it for a limited time
bool Users::set_user_delete(QString login, bool state)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	int tmp_state;
	if (state)
		tmp_state = 1;
	else
		tmp_state = 0;

	query->prepare("UPDATE `users` SET `deleted`=? WHERE login=?");
	query->bindValue(0, tmp_state);
	query->bindValue(1, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	return true;
}

std::vector<QString>* Users::get_user_data(QString login, std::vector<QString>* data)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->prepare("SELECT `email`, `question` FROM `users` WHERE login=?");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	data->clear();
	data->push_back(query->value(0).toString());
	data->push_back(QString::fromStdString(decrypt_msg(query->value(1).toString().toStdString())));

	return data;
}

bool Users::edit_user(QString login, QString password, QString email, QString question, QString answer)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->prepare("UPDATE `users` SET `us_password`=?,`email`=?,`question`=?,`answer`=? WHERE login=?");
	query->bindValue(0, QString::fromStdString(prepare_encrypt(password.toStdString())));
	query->bindValue(1, email);
	query->bindValue(2, QString::fromStdString(prepare_encrypt(question.toStdString())));
	query->bindValue(3, QString::fromStdString(prepare_encrypt(answer.toStdString())));
	query->bindValue(4, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QMessageBox::information(NULL, "User data", "Users data successfully edited!");
	return true;
}

bool Users::edit_password(QString login, QString password)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Encrypting new password
	password = QString::fromStdString(prepare_encrypt<std::string>(password.toStdString()));
	if (password.size() >= 3)
		if (password[0] == '#' && password[1] == '#' && password[2] == '#') {
			std::string err = "Error while encrypting pasword. " + password.toStdString();
			QMessageBox::warning(NULL, "ERROR", QString::fromStdString(err));
			return false;
		}

	//Updating database
	query->prepare("UPDATE `users` SET `us_password`=? WHERE login=?");
	query->bindValue(0, password);
	query->bindValue(1, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QMessageBox::information(NULL, "User data", "Users password successfully edited!");
	return true;
}

//Function to change user type from regular to admin or change admin to regular user
bool Users::change_user_type(QString login)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->prepare("SELECT `login`, `type` FROM `users` WHERE login=?");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	if (!query->next()) {
		QMessageBox::critical(NULL, "ERROR", "Login was not found in database!");
		return false;
	}
	if (query->value(0).toString() == active_user_login) {
		QMessageBox::warning(NULL, "ERROR", "Can`t change your own account type!");
		return false;
	}

	QChar ac_type;
	if (query->value(1).toString() == "u")
		ac_type = 'a';
	else if (query->value(1).toString() == "s") {
		QMessageBox::warning(NULL, "ERROR", "Can`t change superuser account type!");
		return false;
	}	
	else
		ac_type = 'u';

	query->prepare("UPDATE `users` SET `type`=? WHERE login=?");
	query->bindValue(0, ac_type);
	query->bindValue(1, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	return true;
}

//Deleting user form database which are set to be deleted
bool Users::clear_users()
{
	QSqlQuery tmp_query;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	
	query->prepare("UPDATE `users` SET `active_adress`=NULL WHERE deleted=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	query->prepare("SELECT `login` FROM `users` WHERE deleted=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	while (query->next()) {
		extra_query->prepare("SELECT `order_ID` FROM `orders` WHERE `user_login`=?");
		extra_query->bindValue(0, query->value(0).toString());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}

		while (extra_query->next()) {
			tmp_query.prepare("DELETE FROM `amount` WHERE order_ID = ?");
			tmp_query.bindValue(0, extra_query->value(0).toInt());
			if (!tmp_query.exec()) {
				QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
				return false;
			}
		}

		extra_query->prepare("DELETE FROM `orders` WHERE `user_login`=?");
		extra_query->bindValue(0, query->value(0).toString());
		if(!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", extra_query->lastError().text());
			return false;
		}


		tmp_query.prepare("DELETE FROM `adress` WHERE login=?");
		tmp_query.bindValue(0, query->value(0).toString());
		if (!tmp_query.exec()) {
			QMessageBox::critical(NULL, "ERROR", tmp_query.lastError().text());
			return false;
		}

	}
	
	query->prepare("DELETE FROM `users` WHERE deleted=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QMessageBox::information(NULL, "User data", "Users successfully cleared!");
	return true;
}

//Reset question getter
QString Users::get_question(QString login)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return "";
	}
	query->prepare("SELECT `question` FROM `users` WHERE `login`=? AND `deleted`=0");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return "";
	}
	if (query->next()) 
		return QString::fromStdString(decrypt_msg(query->value(0).toString().toStdString()));
	else
		return "";
}

//Active adress getter
int Users::get_user_active_adress()
{
	return active_user_adress;
}

//Function to compare given reset answer with database data
bool Users::compare_answer_questrion(QString login, QString question, QString answer)
{

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->prepare("SELECT `question`, `answer` FROM `users` WHERE `login`=? AND `deleted`=0");
	query->bindValue(0, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	QString right_question, right_answer;
	if (query->next()) {
		right_question = QString::fromStdString(decrypt_msg(query->value(0).toString().toStdString()));
		right_answer = QString::fromStdString(decrypt_msg(query->value(1).toString().toStdString()));

		return right_question == question && right_answer == answer;
	}
	else 
		return false;
	
}

//Function to get order count in specified month
int Users::get_order_count_at(QString login, QDate date)
{

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->prepare("SELECT COUNT(*) FROM `orders` WHERE `order_date`<=? AND `order_date`>=? AND `user_login`=?");
	query->bindValue(0, date.addDays(1).toString("yyyy-MM-dd"));
	query->bindValue(1, date.addMonths(-1).toString("yyyy-MM-dd"));
	query->bindValue(2, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	query->next();
	return query->value(0).toInt();
}

//Function to get ordered products count in specified month
int Users::get_amount_of_products_at(QString login, QDate date)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->prepare("SELECT order_ID FROM `orders` WHERE `order_date`<=? AND `user_login`=?");
	query->bindValue(0, date.addDays(1).toString("yyyy-MM-dd"));
	query->bindValue(1, login);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	
	int sum = 0;
	
	while (query->next()) {
		extra_query->prepare("SELECT SUM(amount) FROM `amount` WHERE order_ID=?");
		extra_query->bindValue(0, query->value(0).toInt());
		if (!extra_query->exec()) {
			QMessageBox::critical(NULL, "ERROR", query->lastError().text());
			return false;
		}
		extra_query->next();
		sum += extra_query->value(0).toInt();
	}

	return sum;
}

//Active user login getter
QString Users::get_active_user_login()
{
	return active_user_login;
}

Users::~Users()
{
}

