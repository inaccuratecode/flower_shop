#include "stdafx.h"
#include "Database_manager.h"


bool Database_manager::Connect_to_db()
{
	connected = database.open();
	qDebug() << "Database connection status = " << connected;
	if (database.lastError().isValid()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!\n" + database.lastError().text());
		connected = false;
		return connected;
	}
	return connected;
}

bool Database_manager::connection_status()
{
	return connected;
}

QSqlDatabase * Database_manager::ptr_to_db()
{
	QSqlDatabase* ptr_do_db = &database;
	return ptr_do_db;
}

QSqlQuery* Database_manager::ptr_to_query()
{
	QSqlQuery* ptr = &query;
	return ptr;
}

QSqlQuery * Database_manager::ptr_to_extra_query()
{
	QSqlQuery* ptr = &extra_query;
	return ptr;
}

Database_manager::Database_manager(QString host_name, QString db_name, int port, QString user_name, QString user_password)
{
	database.setHostName(host_name);
	database.setDatabaseName(db_name);
	database.setPort(port);
	database.setUserName(user_name);

	if (user_password.length() != 0)
		database.setPassword(user_password);
}

Database_manager::~Database_manager()
{
}
