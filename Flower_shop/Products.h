#pragma once
#include "stdafx.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>


class Products
{
	QSqlDatabase* database;
	QSqlQuery* query;
	QSqlQuery* extra_query;
	QString currency;
public:
	Products(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query, QString currency = "$");
	bool get_categories(QListWidget* list);
	bool get_all_products(QListWidget* list);
	bool get_all_products(QListWidget* list, QString name); //overload function - for serch engine
	bool get_category_products(QListWidget* list, QString category);
	bool get_product_photos();
	bool get_basic_info(QListWidget *list);
	bool get_info_from_ID(std::vector<QString>* output, int ID);
	bool add_product(QString name, QString category, float price, QString photo_directory);
	bool edit_product(int ID, QString name, QString category, float price, QString photo_directory);
	bool add_amount(int ID, int quantity);
	bool remove_amount(int ID, unsigned int quantity);
	bool change_insale(int ID, bool in_sale);
	QString get_currency();

	bool test();
};

