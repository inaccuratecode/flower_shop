#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>
#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSqlQuery>
class Database_manager
{
	QSqlDatabase database = QSqlDatabase::addDatabase("QMYSQL");
	QSqlQuery query;
	QSqlQuery extra_query;
	bool connected = false;

public:
	bool Connect_to_db();
	bool connection_status();
	QSqlDatabase* ptr_to_db();
	QSqlQuery* ptr_to_query();
	QSqlQuery* ptr_to_extra_query();
	Database_manager(QString host_name, QString db_name, int port, QString user_name, QString user_password);
	~Database_manager();
};

