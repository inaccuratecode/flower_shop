#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <uchar.h>
#include <sstream>

using namespace std;

static std::string base64_encode(const std::string &in) {

	std::string out;

	int val = 0, valb = -6;
	for (unsigned char c : in) {
		val = (val << 8) + c;
		valb += 8;
		while (valb >= 0) {
			out.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[(val >> valb) & 0x3F]);
			valb -= 6;
		}
	}
	if (valb > -6) out.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[((val << 8) >> (valb + 8)) & 0x3F]);
	while (out.size() % 4) out.push_back('=');
	return out;
}

static std::string base64_decode(const std::string &in) {

	std::string out;

	std::vector<int> T(256, -1);
	for (int i = 0; i < 64; i++) T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;

	int val = 0, valb = -8;
	for (unsigned char c : in) {
		if (T[c] == -1) break;
		val = (val << 6) + T[c];
		valb += 6;
		if (valb >= 0) {
			out.push_back(char((val >> valb) & 0xFF));
			valb -= 8;
		}
	}
	return out;
}

static std::string encryptDecrypt(std::string toEncrypt) {
	char key[8] = { 'V', 't', 'B', 'z', '4', 'o', '2', 'F' }; //Any chars will work
	std::string output = toEncrypt;

	for (int i = 0; i < toEncrypt.size(); i++)
		output[i] = toEncrypt[i] ^ key[i % (sizeof(key) / sizeof(char))];

	return output;
}

template<typename T>
inline std::string prepare_encrypt(T input)
{
	if (is_same<int, T>::value == false && is_same<string, T>::value == false)
		return "### Unsuported data type!";

	fstream tmp;
	string to_encrypt, buffer;

	if (is_same<int, T>::value == true) {

		tmp.open("tmp.k", ios::out | ios::trunc);

		if (tmp.is_open() == false)
			return "### Error while opening tmporary file!";

		tmp << input;
		tmp.close();
		tmp.open("tmp.k", ios::in);
		while (!tmp.eof()) {
			getline(tmp, buffer);
			to_encrypt.append(buffer);
		}
		tmp.close();
		tmp.open("tmp.k", ios::out | ios::trunc);
		tmp.close();
	}
	else
		to_encrypt = input;

	to_encrypt = base64_encode(to_encrypt);
	to_encrypt = encryptDecrypt(to_encrypt);
	return to_encrypt;
}

static std::string decrypt_msg(std::string input) {
	input = encryptDecrypt(input);
	input = base64_decode(input);

	return input;
}