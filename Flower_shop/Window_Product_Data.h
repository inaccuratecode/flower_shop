#pragma once
#include <QDialog>
#include "Products.h"
#include "ui_ProductData.h"
class Window_Product_Data :
	public QDialog
{
	Q_OBJECT

public:
	explicit Window_Product_Data(QDialog *parent = Q_NULLPTR, QString mode = NULL, Products* products_data = NULL, int ID = 1);
private:
	Ui::ProductData ui;
	Products* products_data;
	QString mode;
	int ID;

private slots:
	void on_pushButton_directory_clicked();
	void on_okButton_clicked();
};

