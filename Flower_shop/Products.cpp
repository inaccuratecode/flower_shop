#include "stdafx.h"
#include "Products.h"
#include <QDir>
#include <QResource>

//Main constructor
Products::Products(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query, QString currency)
{
	this->database = database;
	this->query = query;
	this->extra_query = extra_query;
	if (currency.length() != 0)
		this->currency = currency;
}

//Gettin available products categories from database
bool Products::get_categories(QListWidget * list)
{
	QListWidgetItem* itm;

	if (database->isOpen() == false) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->clear();
	query->prepare("SELECT DISTINCT(category) FROM products WHERE in_sale=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	itm = new QListWidgetItem;
	itm->setText("all");
	list->addItem(itm);

	while (query->next()) {
		itm = new QListWidgetItem;
		itm->setText(query->value(0).toString());
		list->addItem(itm);
	}

	return true;
}

//Getting all products from database and putting data in 
bool Products::get_all_products(QListWidget * list)
{
	QListWidgetItem* itm;
	QString directory;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Getting product data from database
	query->clear();
	query->prepare("SELECT `ID`, `name`, `price`, `amount`, `photo` FROM `products` WHERE in_sale=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();

	//Adding photo from downloaded file in Resources directory
	while (query->next()) {
		directory = "Resources/" + query->value(1).toString() + ".jpg";
		itm = new QListWidgetItem;
		itm->setIcon(QIcon(directory));
		itm->setText(query->value(1).toString() + "\nID: " + query->value(0).toString() + "\nPrice: " + query->value(2).toString() + currency + "\nAvailable amount: " + query->value(3).toString());
		list->addItem(itm);
	}
	return true;
}

//Overloaded get_all_product function that applies product name filter
bool Products::get_all_products(QListWidget * list, QString name)
{
	if (name.length() == 0) {
		bool output = get_all_products(list);
		return output;
	}

	QListWidgetItem* itm;
	QString directory;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->clear();
	name = "%" + name + "%";
	query->prepare("SELECT `ID`, `name`, `price`, `amount`, `photo` FROM `products` WHERE in_sale=1 AND LOWER(name) LIKE LOWER(?)");
	query->bindValue(0, name);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		directory = "Resources/" + query->value(1).toString() + ".jpg";
		itm = new QListWidgetItem;
		itm->setIcon(QIcon(directory));
		itm->setText(query->value(1).toString() + "\nID: " + query->value(0).toString() + "\nPrice: " + query->value(2).toString() + currency + "\nAvailable amount: " + query->value(3).toString());
		list->addItem(itm);
	}
	return true;
}

//Overloaded get_all_product function that applies product category filter
bool Products::get_category_products(QListWidget * list, QString category)
{
	QListWidgetItem* itm;
	QString directory;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->clear();

	if (category == "all")
		query->prepare("SELECT `ID`, `name`, `price`, `amount`, `photo` FROM `products` WHERE in_sale=1");
	else {
		query->prepare("SELECT `ID`, `name`, `price`, `amount`, `photo` FROM `products` WHERE in_sale=1 AND category=?");
		query->bindValue(0, category);
	}

	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		directory = "Resources/" + query->value(1).toString() + ".jpg";
		itm = new QListWidgetItem;
		itm->setIcon(QIcon(directory));
		itm->setText(query->value(1).toString() + "\nID: " + query->value(0).toString() + "\nPrice: " + query->value(2).toString() + currency + "\nAvailable amount: " + query->value(3).toString());
		list->addItem(itm);
	}
	return true;
}

//Function for downloading missing product photos from database
bool Products::get_product_photos()
{
	QString photo_name;
	QDir photo_directory;
	QByteArray ba;
	QImage new_flower;
	QDir dir_path;

	//Checking if resources directory exists
	if (!dir_path.exists("Resources")) {
		dir_path.mkdir("Resources");
	}

	//Getting all products names
	query->prepare("SELECT `name` FROM `products` WHERE `in_sale`=1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	//Downloading missing photos of products from database
	while (query->next()) {
		photo_name = "Resources/" + query->value(0).toString() + ".jpg";

		if (photo_directory.exists(photo_name) != false) {
			extra_query->prepare("SELECT `photo` FROM `products` WHERE `name`=?");
			extra_query->bindValue(0, query->value(0).toString());
			if (!extra_query->exec()) {
				QMessageBox::critical(NULL, "ERROR", query->lastError().text());
				return false;
			}
			extra_query->next();

			//Converting photo from byte array to jpg file
			ba.clear();
			ba = extra_query->value(0).toByteArray();
			ba = ba.fromBase64(ba);

			new_flower.loadFromData(ba, "JPG");
			new_flower.save(photo_name, "JPG");
		}
	}
	return true;
}

bool Products::get_basic_info(QListWidget * list)
{
	QListWidgetItem* itm;
	QString answer;

	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	query->clear();
	query->prepare("SELECT `ID`, `name`, `amount`, `in_sale` FROM `products` WHERE 1");
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	list->clear();
	while (query->next()) {
		itm = new QListWidgetItem;

		//Using lambda function to get string answer
		answer = [](int check)->QString {
			if (check == 1) return "Yes";
			else return "No";
		}(query->value(3).toInt());
		
		itm->setText(query->value(1).toString() + "\nID: " + query->value(0).toString() + "\nAvailable amount: " + query->value(2).toString() + "\nIn sale: " + answer + "\n______________________________");
		list->addItem(itm);
	}
	return true;
}

bool Products::get_info_from_ID(std::vector<QString>* output, int ID)
{
	output->clear();
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	query->clear();
	query->prepare("SELECT `name`, `category`, `price` FROM `products` WHERE ID = ?");
	query->bindValue(0, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	query->next();
	output->push_back(query->value(0).toString());
	output->push_back(query->value(1).toString());
	output->push_back(query->value(2).toString());
	output->push_back("Resources/" +query->value(0).toString());

	return true;
}

bool Products::add_product(QString name, QString category, float price, QString photo_directory)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Checking if product name if already in database
	query->prepare("SELECT name FROM products where name=?");
	query->bindValue(0, name);
	query->exec();
	if (query->next()) {
		QMessageBox::warning(NULL, "ERROR", "Product with such name already exists");
		return false;
	}

	QDir chck_path;
	QByteArray ba;
	QBuffer buffer(&ba);

	//Getting photo form given directory and converting it into byte array if there is no photo, the default photo is applied
	if (chck_path.exists(photo_directory)) {
		QImage photo(photo_directory, "JPG");
		photo = photo.scaled(QSize(300, 300), Qt::KeepAspectRatio);
		buffer.open(QIODevice::WriteOnly);
		photo.save(&buffer, "JPG"); //conversion to raw byte data
	}
	else {
		QImage photo(":/Flower_shop/Resources/test kwiatek.jpg", "JPG"); //Default image
		buffer.open(QIODevice::WriteOnly);
		photo.save(&buffer, "JPG"); //conversion to raw byte data
	}

	//Updating database
	query->prepare("INSERT INTO `products`(`ID`, `name`, `category`, `price`, `amount`, `photo`, `in_sale`) VALUES (0, ?, ?, ?, 0, ?, 1)");
	query->bindValue(0, name);
	query->bindValue(1, category);
	query->bindValue(2, price);
	query->bindValue(3, ba.toBase64());
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return "";
	}
	QMessageBox::information(NULL, "Adding product", "New product added!");
	return true;
}

bool Products::edit_product(int ID, QString name, QString category, float price, QString photo_directory)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	QDir chck_path;
	QByteArray ba;
	QBuffer buffer(&ba);

	//Getting photo form given directory and converting it into byte array if there is no photo, the default photo is applied
	if (chck_path.exists(photo_directory)) {
		QImage photo(photo_directory, "JPG");
		photo = photo.scaled(QSize(300, 300), Qt::KeepAspectRatio);
		buffer.open(QIODevice::WriteOnly);
		photo.save(&buffer, "JPG"); //conversion to raw byte data
	}
	else {
		QImage photo(":/Flower_shop/Resources/test kwiatek.jpg", "JPG"); //Default image
		buffer.open(QIODevice::WriteOnly);
		photo.save(&buffer, "JPG"); //conversion to raw byte data
	}

	if (photo_directory != ("Resources/" + name + ".jpg") && chck_path.exists("Resources/" + name + ".jpg")) {
		chck_path.remove("Resources/" + name + ".jpg");
	}

	query->prepare("UPDATE `products` SET `name`=?,`category`=?,`price`=?,`photo`=? WHERE ID = ?");
	query->bindValue(0, name);
	query->bindValue(1, category);
	query->bindValue(2, price);
	query->bindValue(3, ba.toBase64());
	query->bindValue(4, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return "";
	}
	return true;
}

//Updating quantity of selected product
bool Products::add_amount(int ID, int quantity)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Getting current available quantity of product
	query->clear();
	query->prepare("SELECT `amount` FROM `products` WHERE ID=?");
	query->bindValue(0, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	query->next();

	if (query->value(0).toInt() + quantity < 0) {
		QMessageBox::warning(NULL, "ERROR", "Not enough products to finish transaction!");
		return false;
	}

	int new_quantity = quantity + query->value(0).toInt();

	//Updating quantity
	query->prepare("UPDATE `products` SET `amount`=? WHERE ID=?");
	query->bindValue(0, new_quantity);
	query->bindValue(1, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	return true;
}

bool Products::remove_amount(int ID, unsigned int quantity)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}

	//Getting current quantity
	query->clear();
	query->prepare("SELECT `amount` FROM `products` WHERE `ID` = ?");
	query->bindValue(0, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	if (query->value(0).toInt() - quantity < 0) {
		QMessageBox::warning(NULL, "ERROR", "Not enough products to finish transaction!");
		return false;
	}

	int new_quantity = quantity - query->value(0).toInt();

	//Updating current quantity
	query->prepare("UPDATE `products` SET `amount`=? WHERE ID=?");
	query->bindValue(0, new_quantity);
	query->bindValue(1, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}

	return true;
}

//Changing sale status of product
bool Products::change_insale(int ID, bool in_sale)
{
	if (!database->isOpen()) {
		QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!");
		return false;
	}
	
	int new_status;

	if (in_sale)
		new_status = 0;
	else
		new_status = 1;

	//Updating status in database
	query->prepare("UPDATE `products` SET `in_sale`=? WHERE ID=?");
	query->bindValue(0, new_status);
	query->bindValue(1, ID);
	if (!query->exec()) {
		QMessageBox::critical(NULL, "ERROR", query->lastError().text());
		return false;
	}
	return true;
}

//Active currency getter
QString Products::get_currency()
{
	return currency;
}
