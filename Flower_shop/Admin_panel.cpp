#include "stdafx.h"
#include "Admin_panel.h"
#include "Window_userddata.h"
#include "Window_Product_Data.h"


//Constructor that provides access to all data in database through pointers to user_data, products and orders objects
Admin_panel::Admin_panel(QWidget *parent, Users* users_data, QSqlDatabase* database, Products* products, Flower_shop *main_win, Orders* orders)
	: QMainWindow(parent)
{
	this->users_data = users_data;
	this->database = database;
	this->products = products;
	this->orders = orders;
	this->main_win = main_win;

	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::MSWindowsFixedSizeDialogHint);
	this->setWindowTitle("Admin Panel");
	connect(this, SIGNAL(refesh_request(void)), main_win, SLOT(need_to_refresh(void)));
}

//Function to get data from database after logging in
bool Admin_panel::prepare_home()
{
	refresh_lists();
	return true;
}

//Function that gets all products, users, and orders data from database and refreshes all graphic lists in admin panel
void Admin_panel::refresh_lists()
{
	products->get_basic_info(ui.listWidget_products);
	users_data->get_users(ui.listWidget_Users);
	orders->get_orders(ui.listWidget_orders);
}

bool Admin_panel::on_pushButton_addUser_clicked()
{
	Window_userddata data_input(NULL, "add", users_data);
	data_input.setModal(true);
	data_input.exec();
	return true;
}

bool Admin_panel::on_pushButton_addProduct_clicked()
{
	Window_Product_Data data_input(NULL, "add", products);
	data_input.setModal(true);
	data_input.exec();

	//After adding product, the refresh_request is emited to force refresh on product list in main window
	emit refesh_request();
	refresh_lists();
	return true;
}

bool Admin_panel::on_pushButton_EditProduct_clicked()
{
	if (ui.listWidget_products->currentItem() != NULL) {
		//Getting selected product ID from list item
		QString ID;
		ID = ui.listWidget_products->currentItem()->text();
		ID = ID.section("\n", 1, 1);
		ID = ID.section(" ", 1, 1);
		
		Window_Product_Data data_input(NULL, "edit", products, ID.toInt());
		data_input.setModal(true);
		data_input.exec();

		//After editing product, the refresh_request is emited to force refresh on product list in main window
		emit refesh_request();
		refresh_lists();
		return true;
	}
	return false;
}

bool Admin_panel::on_pushButton_supply_clicked()
{
	if (ui.listWidget_products->currentItem() != NULL) {
		QString ID;
		int quantity;

		ID = ui.listWidget_products->currentItem()->text();
		ID = ID.section("\n", 1, 1);
		ID = ID.section(" ", 1, 1);
		quantity = ui.spinBox_supply_amount->value();

		products->add_amount(ID.toInt(), quantity);

		//After updating quantity if product, the refresh_request is emited to force refresh on product list in main window
		emit refesh_request();
		refresh_lists();

		return true;
	}
	return false;
}

//Function that is connected to "change sale status" button. It withdraw product from sale if product is available or make it available if it`s not
bool Admin_panel::on_pushButton_sale_status_clicked()
{
	if (ui.listWidget_products->currentItem() != NULL) {
		QString ID, status;
		bool in_sale;

		//Getting ID of selected product from list item
		ID = ui.listWidget_products->currentItem()->text();
		ID = ID.section("\n", 1, 1);
		ID = ID.section(" ", 1, 1);

		//Getting status of selected product from list item
		status = ui.listWidget_products->currentItem()->text();
		status = status.section("\n", 3, 3);
		status = status.section(" ", 2, 2);

		if (status == "Yes")
			in_sale = true;
		else
			in_sale = false;

		products->change_insale(ID.toInt(), in_sale);

		//After updating status of product, the refresh_request is emited to force refresh on product list in main window
		emit refesh_request();
		refresh_lists();
		return true;
	}
	return false;
}

bool Admin_panel::on_pushButton_change_usr_typ_clicked()
{
	if (ui.listWidget_Users->currentItem() != NULL) {
		QString login;
		login = ui.listWidget_Users->currentItem()->text();
		login = login.section("\n", 0, 0);
		login = login.right(login.size() - 7);

		users_data->change_user_type(login);
		refresh_lists();
		return true;
	}
	return false;
}

bool Admin_panel::on_pushButton_clearUsers_clicked()
{
	users_data->clear_users();
	refresh_lists();
	return true;
}

//Updating Order list if the selected filter was changed
bool Admin_panel::on_comboBox_filter_currentIndexChanged()
{
	orders->get_orders(ui.listWidget_orders, ui.comboBox_filter->currentText());
	return true;
}

//Applying new status to selected order after clicking "change order status" button
bool Admin_panel::on_pushButton_orderstatus_clicked()
{
	if (ui.listWidget_orders->currentItem() != NULL) {
		//Getting ID of order from selected list item
		QString ID = ui.listWidget_orders->currentItem()->text();
		ID = ID.section("\n", 0, 0);
		ID = ID.section(" ", 1, 1);
		
		orders->update_order_status(ID.toInt(), ui.comboBox_order_status->currentText());
		refresh_lists();
		emit refesh_request();
		return true;
	}
	return false;
}
