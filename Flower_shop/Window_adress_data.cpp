#include "stdafx.h"
#include "Window_adress_data.h"

//Main constructor that make sure only edit lines specified by mode are enabled
Window_adress_data::Window_adress_data(QDialog *parent, QString mode, Users* users_data, int ID)
{
	this->mode = mode;
	this->users_data = users_data;
	this->ID = ID;
	ui.setupUi(this);
	if (mode == "edit") {
		std::vector<QString> data;
		users_data->get_adress_by_ID(ID, &data);

		if (data.size() == 4) {
			ui.lineEdit_street->setText(data.at(0));
			ui.lineEdit_postcode->setText(data.at(1));
			ui.lineEdit_city->setText(data.at(2));
			ui.lineEdit_phone->setText(data.at(3));
		}
	}
}
 
bool Window_adress_data::on_okButton_clicked() {
	if (mode == "add") {
		QString tmp_street, tmp_city, tmp_phone;
		int tmp_postcode;
		bool conversion_tel;
		bool conversion_post_code;


		tmp_street = ui.lineEdit_street->text();
		tmp_city = ui.lineEdit_city->text();
		tmp_phone = ui.lineEdit_phone->text();
		tmp_postcode = ui.lineEdit_postcode->text().toInt(&conversion_tel, 10);
		ui.lineEdit_phone->text().toInt(&conversion_tel, 10);

		if (conversion_tel == false || conversion_tel == false || tmp_postcode<0 || tmp_postcode>99999) {
			QMessageBox::warning(this, "ERROR", "Wrong data format!");
			return false;
		}
		else if (tmp_street.size() == 0 || tmp_city.size() == 0 || tmp_phone.size() < 9 || tmp_phone.size() > 14 || tmp_postcode == 0) {
			QMessageBox::warning(this, "ERROR", "Incomplete adress data!");
			return false;
		}

		users_data->add_adress(users_data->get_active_user_login(), tmp_street, tmp_city, tmp_postcode, tmp_phone);
		return true;
	}
	else if (mode == "edit") {
		QString tmp_street, tmp_city, tmp_phone;
		int tmp_postcode;
		bool conversion_tel;
		bool conversion_post_code;


		tmp_street = ui.lineEdit_street->text();
		tmp_city = ui.lineEdit_city->text();
		tmp_phone = ui.lineEdit_phone->text();
		tmp_postcode = ui.lineEdit_postcode->text().toInt(&conversion_tel, 10);
		ui.lineEdit_phone->text().toInt(&conversion_tel, 10);

		if (conversion_tel == false || conversion_tel == false || tmp_postcode < 0 || tmp_postcode>99999) {
			QMessageBox::warning(this, "ERROR", "Wrong data format!");
			return false;
		}
		else if (tmp_street.size() == 0 || tmp_city.size() == 0 || tmp_phone.size() < 9 || tmp_phone.size() > 14 || tmp_postcode == 0) {
			QMessageBox::warning(this, "ERROR", "Incomplete adress data!");
			return false;
		}

		users_data->edit_adress(ID, tmp_street, tmp_city, tmp_postcode, tmp_phone);
		return true;
	}
	return false;
}
