#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Flower_shop.h"
#include "Users.h"
#include "Products.h"
#include "Orders.h"
#include <QSqlDatabase>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

class Flower_shop : public QMainWindow
{
	Q_OBJECT
	Ui::Flower_shopClass ui;
	Users* users_data;
	Products* products;
	Orders* orders;
	QSqlDatabase* database;
	float cart_value = 0.0;

	void refresh_lists();
	bool prepare_chart_bar();
	bool prepare_chart_line();

public:
	Flower_shop(QWidget *parent = Q_NULLPTR, Users* users_data = NULL, QSqlDatabase* database = NULL, Products* products = NULL, Orders* orders=NULL);
	bool prepare_home();

private slots:
	bool on_list_categories_currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
	bool on_pushButton_add_adress_clicked();
	bool on_pushButton_set_active_adress_clicked();
	bool on_pushButton_edit_adress_clicked();
	bool on_pushButton_delete_account_clicked();
	bool on_pushButton_edit_user_clicked();
	bool on_lineEdit_search_textEdited();
	bool on_pushButton_addToCart_clicked();
	bool on_pushButton_delete_cart_clicked();
	bool on_pushButton_finalize_order_clicked();
	void need_to_refresh();

};
