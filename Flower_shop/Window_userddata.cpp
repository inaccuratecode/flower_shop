#include "stdafx.h"
#include "Window_userddata.h"
#include <QMessageBox>

//Main constructor
Window_userddata::Window_userddata(QDialog *parent, QString mode, Users* users_data)
{
	this->mode = mode;
	this->users_data = users_data;
	ui.setupUi(this);

	if (mode == "edit") {
		std::vector<QString> data;
		users_data->get_user_data(users_data->get_active_user_login(), &data);
		ui.lineEdit_login->setEnabled(false);
		if (data.size() == 2) {
			ui.lineEdit_email->setText(data.at(0));
			ui.lineEdit_question->setText(data.at(1));
		}
	}
}

bool Window_userddata::on_okButton_clicked() {
	QString login, password, email, question, answer;
	login = ui.lineEdit_login->text();
	password = ui.lineEdit_password->text();
	email = ui.lineEdit_email->text();
	question = ui.lineEdit_question->text();
	answer = ui.lineEdit_answer->text();

	if (mode == "add") {
		if (login.length() == 0 || password.length() == 0 || email.length() == 0 || question.length() == 0 || answer.length() == 0) {
			QMessageBox::warning(NULL, "ERROR", "Data input is not complete!");
			return false;
		}
		else {
			users_data->add_user(login, password, email, question, answer);
			accept();
		}
	}
	else {
		if (password.length() == 0 || email.length() == 0 || question.length() == 0 || answer.length() == 0) {
			QMessageBox::warning(NULL, "ERROR", "Data input is not complete!");
			return false;
		}
		else {
			users_data->edit_user(users_data->get_active_user_login(), password, email, question, answer);
			accept();
		}
	}
	return true;
}