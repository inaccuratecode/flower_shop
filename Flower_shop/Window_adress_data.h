#pragma once
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include "ui_AdressData.h"
#include "Users.h"
class Window_adress_data :
	public QDialog
{
	Q_OBJECT

public:
	explicit Window_adress_data(QDialog *parent = Q_NULLPTR, QString mode = NULL, Users* users_data = NULL, int ID = 0);

private:
	Ui::AdressData ui;
	Users* users_data;
	QString mode;
	int ID;

private slots:
	bool on_okButton_clicked();
};

