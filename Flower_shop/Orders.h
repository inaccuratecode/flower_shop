#pragma once
#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>
#include <QDebug>
#include <QSqlQuery>
#include <Users.h>
class Orders
{
	QSqlDatabase* database;
	QSqlQuery* query;
	QSqlQuery* extra_query;
	Users* users_data;
public:
	bool get_orders(QListWidget* list);
	bool get_orders(QListWidget* list, QString filter);
	bool get_user_orders(QListWidget* list, QString login);
	bool order_from_cart(QListWidget* list, float cart_value);
	bool update_order_status(int ID, QString status);
	Orders(QSqlDatabase* database, QSqlQuery* query, QSqlQuery* extra_query, Users* users_data);
	~Orders();
};

