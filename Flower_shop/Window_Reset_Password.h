#pragma once
#include <QDialog>
#include <QString>
#include <QSqlDatabase>
#include <QtSql>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include "ui_ResetPasswordWindow.h"
#include "Users.h"
class Window_Reset_Password :
	public QDialog
{
	Q_OBJECT

public:
	explicit Window_Reset_Password(QDialog *parent = Q_NULLPTR, Users* user_data = NULL , QString login = NULL);

private:
	Ui::ResetPasswordWindow ui;
	Users* users_data;
	QString login;


private slots:
	void on_okButton_clicked();
	void on_cancelButton_clicked();
};