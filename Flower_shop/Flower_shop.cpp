﻿#include "stdafx.h"
#include "Flower_shop.h"
#include "Window_adress_data.h"
#include "Window_userddata.h"
#include <QLocale>

QT_CHARTS_USE_NAMESPACE

//Constructor that provides access to all data in database through pointers to user_data, products and orders objects
Flower_shop::Flower_shop(QWidget *parent, Users* users_data, QSqlDatabase* database, Products* products, Orders* orders)
	: QMainWindow(parent)
{
	this->users_data = users_data;
	this->database = database;
	this->products = products;
	this->orders = orders;
	ui.setupUi(this);

	this->setWindowTitle("Flower shop");
	ui.lineEdit_search->setPlaceholderText("Search");
	setWindowFlags(Qt::Window | Qt::MSWindowsFixedSizeDialogHint);

	ui.list_products->setIconSize(QSize(250, 250));
	ui.listWidget_cart->setIconSize(QSize(100, 100));
}

//Function to refresh data in all graphic lists in main window
void Flower_shop::refresh_lists()
{
	products->get_categories(ui.list_categories);
	products->get_product_photos();
	products->get_all_products(ui.list_products);
	users_data->get_user_adresses(users_data->get_active_user_login(), ui.listWidget_adress);
	orders->get_user_orders(ui.listWidget_orders, users_data->get_active_user_login());
}

//Setup function to get data from database after succesfull login
bool Flower_shop::prepare_home()
{
	refresh_lists();
	prepare_chart_bar();
	prepare_chart_line();
	return true;
}

bool Flower_shop::on_pushButton_add_adress_clicked()
{
	//Initializing window to add adress data and setting modal to prevent the change of other data while adding new postal data
	Window_adress_data adress_data(NULL, "add", users_data);
	adress_data.setModal(true);
	adress_data.show();
	adress_data.exec();
	
	refresh_lists();
	return false;
}

//Updating current user active postal data in database
bool Flower_shop::on_pushButton_set_active_adress_clicked()
{
	if (ui.listWidget_adress->currentItem() != NULL) {
		QString tmp_ID = ui.listWidget_adress->currentItem()->text();

		if (tmp_ID.section("\n", 0, 0) == "\tACTIVE ADRESS:") {
			QMessageBox::warning(NULL, "ERROR", "This is already Your active adress!");
			return false;
		}
		tmp_ID = tmp_ID.section("\n", 0, 0);
		tmp_ID = tmp_ID.section(" ", 2, 2);

		users_data->update_active_adress(users_data->get_active_user_login(), tmp_ID.toInt());
		refresh_lists();
		return true;
	}
	return false;
}

bool Flower_shop::on_pushButton_edit_adress_clicked()
{
	if (ui.listWidget_adress->currentItem() != NULL) {
		QString tmp_ID = ui.listWidget_adress->currentItem()->text();
		if (tmp_ID.section("\n", 0, 0) == "\tACTIVE ADRESS:") {
			tmp_ID = tmp_ID.section("\n", 1, 1);
			tmp_ID = tmp_ID.section(" ", 2, 2);
		}
		else {
			tmp_ID = tmp_ID.section("\n", 0, 0);
			tmp_ID = tmp_ID.section(" ", 2, 2);
		}

		//Initializing window for changing selected adress and setting modal to prevent user from changing other data while editing postal info
		Window_adress_data adress_data(NULL, "edit", users_data, tmp_ID.toInt());
		adress_data.setModal(true);
		adress_data.show();
		adress_data.exec();

		refresh_lists();
		return true;
	}
	return false;
}

//Button to delete active account
bool Flower_shop::on_pushButton_delete_account_clicked()
{
	//Preventing from deleting superuser account
	if (users_data->get_user_type() == 's') {
		QMessageBox::warning(this, "Error", "Can`t delete superuser account!");
		return false;
	}

	//Additional message box to make sure that user has`t pressed button accidentally
	QMessageBox::StandardButton reply = QMessageBox::question(this, "Delete data", "You are going to delete your data from database. Do you want to Continue?", QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes) {
		users_data->set_user_delete(users_data->get_active_user_login(), true);
		QMessageBox::information(this, "Delete Data", "Your data is set to be deleted from database. You can still recover your data. \nIf you want your data be deleted immeditly, contact administrator.");
		this->close();
		return true;
	}
	else
		return false;
}

bool Flower_shop::on_pushButton_edit_user_clicked()
{
	//Initializing edit user data window with modal for preventing user from editing other data while editing account information
	Window_userddata data(NULL, "edit", users_data);
	data.setModal(true);
	data.show();
	data.exec();
	refresh_lists();
	return true;
}

//Updating list of product if search line was edited 
bool Flower_shop::on_lineEdit_search_textEdited()
{
	products->get_all_products(ui.list_products, ui.lineEdit_search->text());
	return true;
}

//Adding products to cart
bool Flower_shop::on_pushButton_addToCart_clicked()
{
	if (ui.list_products->currentItem() != NULL) {

		QString text = ui.list_products->currentItem()->text();
		text = text.section("\n", 0, 0);
		QString product_name;

		//Loop that goes through the cart to make sure that product is not already in cart
		for (int x = 0; x < ui.listWidget_cart->count(); x++) {
			product_name = ui.listWidget_cart->item(x)->text();
			product_name = product_name.section("\n", 0, 0);
			if (text == product_name) {
				QMessageBox::warning(NULL, "ERROR", "Product is already in cart!");
				return false;
			}
		}

		//Checking if there is enough products in stock
		text = ui.list_products->currentItem()->text();
		text = text.section("\n",3,3);
		text = text.section(" ", 2, 2);

		if (text.toInt() < ui.spinBox_amount->value()) {
			QMessageBox::warning(NULL, "ERROR", "Not enough products on stock!");
			return false;
		}
		else {
			//Calculating value of product in cart
			QString price_pice = ui.list_products->currentItem()->text();
			QString tmp_price;
			price_pice = price_pice.section("\n", 2, 2);
			price_pice = price_pice.section(" ", 1, 1);
			price_pice.chop(products->get_currency().size());

			//Updating text boxes with product value information
			cart_value += price_pice.toFloat()*ui.spinBox_amount->value();

			text = ui.list_products->currentItem()->text();
			text = text.section("\n", 0, 1);
			text.append("\n");
			text.append("Ordered amount: " + QString::fromStdString(std::to_string(ui.spinBox_amount->value())));
			text.append("\n");
			tmp_price = QString::fromStdString(std::to_string(ui.spinBox_amount->value()*price_pice.toFloat()));
			while (tmp_price.endsWith('0')) {
				tmp_price.chop(1);
			}
			if(tmp_price.endsWith('.') )
				tmp_price.chop(1);
			text.append("Position price: " + tmp_price + products->get_currency());
			QListWidgetItem *itm = ui.list_products->currentItem()->clone();

			itm->setText(text);
			ui.listWidget_cart->addItem(itm);
			tmp_price = QString::fromStdString(std::to_string(cart_value));
			while (tmp_price.endsWith('0') == true) {
				tmp_price.chop(1);
			}
			if (tmp_price.endsWith('.') == true)
				tmp_price.chop(1);
			ui.label_cart_value->setText("Cart value: " + tmp_price + products->get_currency());
		}
		return true;
	}
	return false;
}

//Deleting selected product from cart
bool Flower_shop::on_pushButton_delete_cart_clicked()
{
	if (ui.listWidget_cart->currentItem() != NULL) {
		QString text = ui.listWidget_cart->currentItem()->text();
		text = text.section("\n", 3, 3);
		text = text.section(" ", 2, 2);
		text.chop(products->get_currency().size());

		cart_value -= text.toFloat();
		QString tmp_price = QString::fromStdString(std::to_string(cart_value));
		while (tmp_price.endsWith('0')) {
			tmp_price.chop(1);
		}
		if (tmp_price.endsWith('.'))
			tmp_price.chop(1);
		ui.label_cart_value->setText("Cart value: " + tmp_price + products->get_currency());
		QListWidgetItem* itm = ui.listWidget_cart->takeItem(ui.listWidget_cart->currentRow());
		delete itm;
	}
	return false;
}


bool Flower_shop::on_pushButton_finalize_order_clicked()
{
	//Calling method from orders object to generate order
	bool output = orders->order_from_cart(ui.listWidget_cart, cart_value);

	if (output) {
		ui.label_cart_value->setText("Cart value: 0" + products->get_currency());
		refresh_lists();
		cart_value = 0;
		return true;
	}
	else
		return false;
}

void Flower_shop::need_to_refresh()
{
	refresh_lists();
}

//Function to generate statistics bar chart for order count
bool Flower_shop::prepare_chart_bar()
{
	QLocale curLocale(QLocale(QLocale::English, QLocale::EuropeanUnion));
	QLocale::setDefault(curLocale);

	QDate date, curr_date;
	curr_date = curr_date.currentDate();
	curr_date = curr_date.addMonths(1);
	date.setDate(curr_date.year(), curr_date.month(), 1);
	date = date.addDays(-1);
	
	//Getting last 6 months names
	QBarSet* month6 =(new QBarSet(curLocale.toString(date,"MMMM")));
	date = date.addMonths(-1);
	QBarSet* month5 = (new QBarSet(curLocale.toString(date, "MMMM")));
	date = date.addMonths(-1);
	QBarSet* month4 = (new QBarSet(curLocale.toString(date, "MMMM")));
	date = date.addMonths(-1);
	QBarSet* month3 = (new QBarSet(curLocale.toString(date, "MMMM")));
	date = date.addMonths(-1);
	QBarSet* month2 = (new QBarSet(curLocale.toString(date, "MMMM")));
	date = date.addMonths(-1);
	QBarSet* month1 = (new QBarSet(curLocale.toString(date, "MMMM")));

	//Getting order count from database for last 6 months
	date = date.addMonths(5);
	int order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	*month6 << order_count;
	date = date.addMonths(-1);
	int tmp_order_count = order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	if (order_count < tmp_order_count)
		order_count = tmp_order_count;
	*month5 << tmp_order_count;
	date = date.addMonths(-1);
	tmp_order_count = order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	if (order_count < tmp_order_count)
		order_count = tmp_order_count;
	*month4 << tmp_order_count;
	date = date.addMonths(-1);
	tmp_order_count = order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	if (order_count < tmp_order_count)
		order_count = tmp_order_count;
	*month3 << tmp_order_count;
	date = date.addMonths(-1);
	tmp_order_count = order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	if (order_count < tmp_order_count)
		order_count = tmp_order_count;
	*month2 << tmp_order_count;
	date = date.addMonths(-1);
	tmp_order_count = order_count = users_data->get_order_count_at(users_data->get_active_user_login(), date);
	if (order_count < tmp_order_count)
		order_count = tmp_order_count;
	*month1 << tmp_order_count;

	//Preparing axis scale
	int tick_amount = order_count / 10;
	if (tick_amount == 0)
		tick_amount = 1;

	//Applying data to chart
	QBarSeries *series = new QBarSeries;
	series->append(month6);
	series->append(month5);
	series->append(month4);
	series->append(month3);
	series->append(month2);
	series->append(month1);

	QChart *chart = new QChart;
	chart->addSeries(series);
	chart->setTitle("Number of orders in last 6 months");
	chart->setAnimationOptions(QChart::AllAnimations);
	chart->legend()->setVisible(true);
	chart->createDefaultAxes();
	chart->axisX()->setVisible(false);
	QValueAxis *axisY = new QValueAxis;

	//Fitting axis sizes
	axisY->setTickInterval(tick_amount);
	axisY->setLabelFormat("%d");
	axisY->setTickAnchor(0);
	axisY->setTickType(QValueAxis::TicksDynamic);
	chart->setAxisY(axisY, series);
	QChartView *chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);
	ui.tabWidget->insertTab(5, chartView, "Stat- Order Count");
	return true;
}

//Function to generate statistics line chart for number of ordered product
bool Flower_shop::prepare_chart_line()
{
	QLocale curLocale(QLocale(QLocale::English, QLocale::EuropeanUnion));
	QLocale::setDefault(curLocale);

	QDate date, curr_date;
	curr_date = curr_date.currentDate();
	curr_date = curr_date.addMonths(1);
	date.setDate(curr_date.year(), curr_date.month(), 1);
	date = date.addDays(-1);

	//Getting names of last 6 months
	QLineSeries *series = new QLineSeries();
	date = date.addMonths(-5);
	series->append(0, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));
	date = date.addMonths(1);
	series->append(1, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));
	date = date.addMonths(1);
	series->append(2, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));
	date = date.addMonths(1);
	series->append(3, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));
	date = date.addMonths(1);
	series->append(4, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));
	date = date.addMonths(1);
	series->append(5, users_data->get_amount_of_products_at(users_data->get_active_user_login(), date));

	//Preparing axis scale
	int tick_amount = users_data->get_amount_of_products_at(users_data->get_active_user_login(), date) / 10;
	if (tick_amount == 0)
		tick_amount = 1;

	QChart *chart = new QChart();
	chart->legend()->hide();
	chart->addSeries(series);
	QValueAxis *axisY = new QValueAxis;

	//Fiting axises size
	axisY->setTickInterval(tick_amount);
	axisY->setLabelFormat("%d");
	axisY->setTickAnchor(0);
	axisY->setTickType(QValueAxis::TicksDynamic);
	chart->setAxisY(axisY, series);

	chart->setTitle("Sum of products ordered by month");

	chart->setAnimationOptions(QChart::AllAnimations);

	// Change the x axis categories
	QCategoryAxis *axisX = new QCategoryAxis();
	date = date.addMonths(-5);
	axisX->append(curLocale.toString(date, "MMMM"), 0);
	date = date.addMonths(1);
	axisX->append(curLocale.toString(date, "MMMM"), 1);
	date = date.addMonths(1);
	axisX->append(curLocale.toString(date, "MMMM"), 2);
	date = date.addMonths(1);
	axisX->append(curLocale.toString(date, "MMMM"), 3);
	date = date.addMonths(1);
	axisX->append(curLocale.toString(date, "MMMM"), 4);
	date = date.addMonths(1);
	axisX->append(curLocale.toString(date, "MMMM"), 5);
	chart->setAxisX(axisX, series);
	

	// Displaing the chart
	QChartView *chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);
	ui.tabWidget->insertTab(5, chartView, "Stat- products count");
	return true;
}

//Function to update the list of products if the category was changed
bool Flower_shop::on_list_categories_currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous) {
	if (current != NULL) {
		QString category = current->text();
		bool output = products->get_category_products(ui.list_products, category);
		return output;
	}
	return false;
}
