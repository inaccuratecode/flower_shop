#pragma once

#include <QDialog>
#include <QSqlDatabase>
#include <QtSql>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include "ui_LoginWindow.h"
#include "Users.h"


class Login_window :
	public QDialog
{
	Q_OBJECT

public:
	explicit Login_window(QDialog *parent = Q_NULLPTR, Users* users_data = nullptr);

private:
	Ui::Form ui;
	Users* users_data = NULL;

protected:
	void closeEvent(QCloseEvent * e);

private slots:
	void on_LoginButton_2_clicked();
	void on_pushButton_resetpassword_2_clicked();
	void on_pushButton_resetpassword_answer_clicked();
	void on_pushButton_recovery_2_clicked();
};

