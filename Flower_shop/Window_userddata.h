#pragma once
#include <QDialog>
#include "ui_UserData.h"
#include "Users.h"
class Window_userddata : public QDialog
{
	Q_OBJECT

public:
	explicit Window_userddata(QDialog *parent = Q_NULLPTR, QString mode = NULL, Users* users_data = NULL);

private:
	Ui::UserData ui;
	Users* users_data;
	QString mode;

private slots:
	bool on_okButton_clicked();
};

