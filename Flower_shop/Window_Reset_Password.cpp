#include "stdafx.h"
#include "Window_Reset_Password.h"

//Main constructor
Window_Reset_Password::Window_Reset_Password(QDialog *parent, Users* user_data, QString login)
	: QDialog(parent)
{
	this->users_data = user_data;
	this->login = login;
	ui.setupUi(this);
	this->setWindowTitle("Password reset");
}

void Window_Reset_Password::on_okButton_clicked()
{
	QString tmp_password, confirm;
	
	tmp_password = ui.lineEdit_password->text();
	confirm = ui.lineEdit_confirm->text();

	if (tmp_password != confirm) {
		QMessageBox::warning(NULL, "Can`t set new password", "Password and confirmation lines do not mach.");
		return;
	}
	else {
		users_data->edit_password(login, tmp_password);
		accept();
	}
	return;
}

void Window_Reset_Password::on_cancelButton_clicked()
{
	reject();
}

