#pragma once
#include <QtWidgets/QMainWindow>
#include "ui_AdminPanel.h"
#include "Users.h"
#include "Products.h"
#include "Orders.h"
#include "Flower_shop.h"

class Admin_panel : public QMainWindow
{
	Q_OBJECT
public:
	Admin_panel(QWidget *parent = Q_NULLPTR, Users* users_data = NULL, QSqlDatabase* database = NULL, Products* products = NULL, Flower_shop *main_win = NULL, Orders* orders=NULL);
	
	//Pointers to objects which allow to update data in database
	Users* users_data;
	Products* products;
	Orders* orders;
	QSqlDatabase* database;

	bool prepare_home();

private:
	Ui::AdminPanel ui;
	Flower_shop *main_win;
	void refresh_lists();
	
private slots:
	bool on_pushButton_addUser_clicked();
	bool on_pushButton_addProduct_clicked();
	bool on_pushButton_EditProduct_clicked();
	bool on_pushButton_supply_clicked();
	bool on_pushButton_sale_status_clicked();
	bool on_pushButton_change_usr_typ_clicked();
	bool on_pushButton_clearUsers_clicked();
	bool on_comboBox_filter_currentIndexChanged();
	bool on_pushButton_orderstatus_clicked();
signals:
	void refesh_request();
};

