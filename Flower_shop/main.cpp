
 //Made by Jan Kie�



#include "stdafx.h"
#include "Flower_shop.h"
#include "Login_window.h"
#include "Admin_panel.h"
#include "Users.h"
#include "Products.h"
#include "Orders.h"
#include "Cypher_Decypher.h"
#include <QtWidgets/QApplication>
#include "Database_manager.h"


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);


	//Database manager initialization
	database_manager database("localhost", "flower_shop", 3306, "root", "");
	
	//Connecting to selected database 
	database.Connect_to_db();
	if (!database.connection_status()){
			QMessageBox::critical(NULL, "ERROR", "Unable to connect to database!\n" + database.ptr_to_db()->lastError().text());
			return -1;
	}


	//Initializing objects to manage users, products and orders
	Users users_data(database.ptr_to_db(), database.ptr_to_query(), database.ptr_to_extra_query());
	Products products(database.ptr_to_db(), database.ptr_to_query(), database.ptr_to_extra_query());
	products.get_product_photos();
	Orders orders_data(database.ptr_to_db(), database.ptr_to_query(), database.ptr_to_extra_query(), &users_data);

	//Initializing main window and login window
	Flower_shop shop_window(NULL, &users_data, database.ptr_to_db(), &products, &orders_data);
	Admin_panel admin_panel(&shop_window, &users_data, database.ptr_to_db(), &products, &shop_window, &orders_data);
	Login_window login_window(NULL, &users_data);
	login_window.show();
	login_window.exec();
	

	//If login was successful giving access to main window
	if (users_data.login_status()) {
		shop_window.show();
		shop_window.prepare_home();

		//If logged user is admin or superuser also giving access to admin panel
		if (users_data.get_user_type() == 'a' || users_data.get_user_type() == 's') {
			admin_panel.show();
			admin_panel.prepare_home();
		}
		app.exec();
	}

	return 0;
}
