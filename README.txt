﻿Designed and implemented by Jan Kieś

Flower Shop is student project that represents basic implementation 
of desktop app for flower shop.

#############Project requierments:
-Creating console or desktop app that represents basic implementation of flower shop app.
-lambda function (can be found in Products::get_basic_info function)
-at least one object from std lib (vector can be found in Users::get_adress_by_ID function)
-saving data using database or files
-customer accounts are created by administrator
-customers are able to place orders
-order status can be changed by administrator
-clients can view their history of previous orders
-calculating at least 2 statistics

#############Project setup:
To make app works the user needs to follow given steps:
1.Download the content of repository
2.Import database SQL file (flower_shop.sql), that can be found in #Resources folder, 
to any kind of local SQL server, for example using Xampp 
(https://www.apachefriends.org/pl/index.html).
3.After importing ant turning on server host, it can be runned from 
Flower_shop.exe that can be found in x64 folder.
4.To test app use one of the example users accounts:
Admin example:
	login: superuser
	password: adfg

User example:
	login: kasztan
	password: 123


#############Implementation desctiption:
The app was written in C++ using QT framework. Aplication meets basic requierments 
and also provides additional functionality. Project represents basic concept of 
desktop app for shoping online (shop customers) and order management. 

To store the data of users, products and order data aplication uses mySQL database created and
implemented especially for this purpose. Sensitive data in database are encrypted by using 
light example encryption algorithm used in app. The app is also sql injection-proof, 
due to using query.bindValue() method, which prevents from injecting malicious
sql commands by unauthorised users.
The structure of database is visualized in graphic file that can be 
found in #Resources folder.

First of all the application is connected to database. Then main objects used to
manage users and products data are initialized. After that get_product_photos() method is called.
Each of product can contain their own photo. 
Photos are stored in database as Byte arrays. This method checks if photo of 
every product is downloaded to local resources. Downloading photos 
ensures fast access. Gaining them from database after every list widget refresh
could slow down application performance. 
Missing photos are downloaded and converted to jpg files while starting app.

If the app conects to database successfully, users first encounter login screen.
It includes three main tabs: login, password reset and account recovery. 
User can delete their account in main window, however the user data is 
not deleted immediately. By using account recovery user can regain acess to their account. 
Password reset allows user to reset their password in case of forgeting it. 
It occures by using question system. While creating account, user creates their
own reset question and answer. After logging in user gain acess to shop main window.

Main window contains list widget with all products available in shop. 
User can browse product and find basic info about them. 
There are options to filter products by category or use search line to
find product with specified name. The search line is case insensitive owning to
use regular expression and LOWER() function in SQL query. User can
add products to cart what allows to make orders with multiply products. 

In cart tab user can see summary value of cart, delte selected products 
from cart or finalize order.

In account tab users can add new post adress. Every user have possibility of having several 
post adreses saved in database, but only one can be the active one. 
Active adress can be selected in account tab. User is able to edit all saved adresses.
User has an access to edit account data and aforementioned delete account option.
Superuser is not allowed to delete account. It prevents loosing acess to administrator functions.

In orders tab user can browse order history with content 
status and other data included in order description.

Two last tabs are user statistics. First tab counts the sum of orders in each month separately
and displays it as a bar chart. 
Second tab counts the sum of orders in last 6 months together and displays it as a line chart.
Each chart is updated and generated while logging in.
The scale and axies size are calculated to fit maximal value properly.

If logged user type is administrator or superuser, appart form main 
window they get access to admin panel. In administrator panel can be found home tab that 
is used to add new users, clear database from accounts set to be
deletes or changes user type from regular user to administrator.

In orders tab administrators can browse all orders with option of filter them by type. 
They can also change orders status.

In last tab administrators have access to managment options of all products. 
There is possibility to add new products, edit existing one, updating available amount of them, 
withdraw product from shop or make it available again.

The app contains dialog boxes which inform user about significant information or errors.
